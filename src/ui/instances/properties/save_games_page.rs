use super::{InstancePropertiesWindow, SaveGameRow};
use crate::gobject::{GInstance, GSaveGame};
use crate::utils::{error_dialog, open_path, spawn_tokio_future, ListStoreExt};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::SaveGame;
use cobble_core::Instance;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/save_games_page.ui"
    )]
    pub struct SaveGamesInstancePropertiesPage {
        #[template_child]
        pub open_save_folder_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub saves_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub saves_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub saves: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SaveGamesInstancePropertiesPage {
        const NAME: &'static str = "SaveGamesInstancePropertiesPage";
        type Type = super::SaveGamesInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SaveGamesInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::SaveGamesInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::SaveGamesInstancePropertiesPage::SAVES,
                        "Saves",
                        "Saves",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::SaveGamesInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::SaveGamesInstancePropertiesPage::SAVES => {
                    *self.saves.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::SaveGamesInstancePropertiesPage::INSTANCE => {
                    self.instance.borrow().to_value()
                }
                super::SaveGamesInstancePropertiesPage::SAVES => self.saves.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for SaveGamesInstancePropertiesPage {}
    impl PreferencesPageImpl for SaveGamesInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct SaveGamesInstancePropertiesPage(ObjectSubclass<imp::SaveGamesInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl SaveGamesInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const SAVES: &str = "saves";

    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        // Bind ListBox
        self.imp().saves_listbox.bind_model(
            Some(&self.save_games()),
            clone!(@weak self as this => @default-panic, move |object| {
                let save = object.downcast_ref::<GSaveGame>().unwrap();
                SaveGameRow::new(save, &this).upcast()
            }),
        );
    }

    #[template_callback]
    fn on_open_folder(&self) {
        let instance = Instance::from(self.instance());
        open_path(instance.save_games_path());
    }

    pub fn load_save_games(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let save_games_result = spawn_tokio_future(async move {instance.load_save_games().await}).await;
            match save_games_result {
                Ok(save_games) => {
                    this.set_save_games(save_games);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_save_games(&self, mut saves: Vec<SaveGame>) {
        // Sort saves by name
        saves.sort_unstable_by(|a, b| a.name.cmp(&b.name));

        // Set saves
        let saves = saves.into_iter().map(GSaveGame::from).collect::<Vec<_>>();
        self.save_games()
            .splice(0, self.save_games().n_items(), &saves);

        self.update_view();
    }

    pub async fn remove_save_game(&self, save_game: GSaveGame) {
        let index = self
            .save_games()
            .index_of::<_, GSaveGame>(|x| save_game.path() == x.path());
        match index {
            Some(i) => {
                let save_game = SaveGame::from(save_game);
                let result = spawn_tokio_future(save_game.remove()).await;

                match result {
                    Ok(_) => self.save_games().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown save game"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.save_games().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::SaveGames),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.saves_stack.set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn save_games(&self) -> ListStore {
        self.property(Self::SAVES)
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    SaveGames,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::SaveGames => "save-games",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
