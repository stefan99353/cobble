use crate::gobject::{GInstance, GLogFile};
use crate::utils::{error_dialog, spawn_tokio_future};
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::traits::ComboRowExt;
use cobble_core::minecraft::LogFile;
use cobble_core::Instance;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject};
use gtk::SignalListItemFactory;
use once_cell::sync::Lazy;
use sourceview5::traits::BufferExt;
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/log_files_page.ui"
    )]
    pub struct LogFilesInstancePropertiesPage {
        #[template_child]
        pub file_comborow: TemplateChild<adw::ComboRow>,
        #[template_child]
        pub sourceview: TemplateChild<sourceview5::View>,
        #[template_child]
        pub sourcemap: TemplateChild<sourceview5::Map>,

        pub instance: RefCell<GInstance>,
        pub log_files: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LogFilesInstancePropertiesPage {
        const NAME: &'static str = "LogFilesInstancePropertiesPage";
        type Type = super::LogFilesInstancePropertiesPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LogFilesInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::LogFilesInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::LogFilesInstancePropertiesPage::LOG_FILES,
                        "Log Files",
                        "Log Files",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::LogFilesInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::LogFilesInstancePropertiesPage::LOG_FILES => {
                    *self.log_files.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::LogFilesInstancePropertiesPage::INSTANCE => {
                    self.instance.borrow().to_value()
                }
                super::LogFilesInstancePropertiesPage::LOG_FILES => {
                    self.log_files.borrow().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for LogFilesInstancePropertiesPage {}
    impl BoxImpl for LogFilesInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct LogFilesInstancePropertiesPage(ObjectSubclass<imp::LogFilesInstancePropertiesPage>)
    @extends gtk::Widget, gtk::Box;
}

#[gtk::template_callbacks]
impl LogFilesInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const LOG_FILES: &str = "log-files";

    fn setup_widgets(&self) {
        let imp = self.imp();

        // Bind ComboRow
        imp.file_comborow.set_factory(Some(&list_factory()));
        imp.file_comborow.set_model(Some(&self.log_files()));

        let buffer = sourceview5::Buffer::new(None);
        buffer_style(&buffer);
        imp.sourceview.set_buffer(Some(&buffer));
    }

    #[template_callback]
    fn on_file_selected(&self) {
        let imp = self.imp();

        let log_file = imp
            .file_comborow
            .selected_item()
            .map(|l| l.downcast::<GLogFile>().unwrap());

        if let Some(log_file) = log_file {
            self.load_file(log_file);
        }
    }

    pub fn load_log_files(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let log_files_result = spawn_tokio_future(async move {instance.load_log_files().await}).await;
            match log_files_result {
                Ok(log_files) => {
                    this.set_log_files(log_files);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_log_files(&self, mut log_files: Vec<LogFile>) {
        log_files.sort_unstable_by(|a, b| b.modified.cmp(&a.modified));
        let log_files = log_files
            .into_iter()
            .map(GLogFile::from)
            .collect::<Vec<_>>();
        self.log_files()
            .splice(0, self.log_files().n_items(), &log_files);
    }

    fn load_file(&self, log_file: GLogFile) {
        let buffer = sourceview5::Buffer::new(None);
        buffer_style(&buffer);

        let file = gio::File::for_path(log_file.path());
        let file = sourceview5::File::builder().location(&file).build();

        let loader = sourceview5::FileLoader::new(&buffer, &file);
        loader.load_async(glib::PRIORITY_DEFAULT, gio::Cancellable::NONE, |res| {
            if let Err(err) = res {
                error!("{err}");
            }
        });

        self.imp().sourceview.set_buffer(Some(&buffer));
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn log_files(&self) -> ListStore {
        self.property(Self::LOG_FILES)
    }
}

fn list_factory() -> SignalListItemFactory {
    let factory = SignalListItemFactory::new();

    factory.connect_bind(move |_, item| {
        let item = item.clone().downcast::<gtk::ListItem>().unwrap();
        let log_file = item.item().unwrap().downcast::<GLogFile>().unwrap();

        let label = gtk::Label::new(Some(&log_file.name()));
        label.set_valign(gtk::Align::Center);
        label.set_halign(gtk::Align::Start);
        item.set_child(Some(&label));
    });

    factory
}

fn buffer_style(buffer: &sourceview5::Buffer) {
    let style_manager = adw::StyleManager::default();
    let scheme = if style_manager.is_dark() {
        sourceview5::StyleSchemeManager::new().scheme("Adwaita-dark")
    } else {
        sourceview5::StyleSchemeManager::new().scheme("Adwaita")
    };

    if let Some(scheme) = &scheme {
        buffer.set_style_scheme(Some(scheme));
    }
}
