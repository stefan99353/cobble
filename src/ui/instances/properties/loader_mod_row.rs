use super::FabricInstancePropertiesPage;
use crate::gobject::GLoaderMod;
use crate::paths;
use crate::utils::{error_dialog, spawn_tokio_future};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::LoaderMod;
use gdk::gdk_pixbuf::Pixbuf;
use gettextrs::gettext;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, Priority};
use once_cell::sync::{Lazy, OnceCell};
use sha1::{Digest, Sha1};
use std::path::PathBuf;
use tokio::fs::create_dir_all;
use tokio::io::AsyncWriteExt;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/loader_mod_row.ui"
    )]
    pub struct LoaderModRow {
        #[template_child]
        pub icon_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub description_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub enabled_switch: TemplateChild<gtk::Switch>,

        pub loader_mod: OnceCell<GLoaderMod>,
        pub page: OnceCell<FabricInstancePropertiesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LoaderModRow {
        const NAME: &'static str = "LoaderModRow";
        type Type = super::LoaderModRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LoaderModRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::LoaderModRow::LOADER_MOD,
                        "Mod",
                        "Mod",
                        GLoaderMod::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecObject::new(
                        super::LoaderModRow::PAGE,
                        "Page",
                        "Page",
                        FabricInstancePropertiesPage::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::LoaderModRow::LOADER_MOD => {
                    self.loader_mod.set(value.get().unwrap()).unwrap()
                }
                super::LoaderModRow::PAGE => self.page.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::LoaderModRow::LOADER_MOD => self.loader_mod.get().to_value(),
                super::LoaderModRow::PAGE => self.page.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for LoaderModRow {}
    impl ListBoxRowImpl for LoaderModRow {}
}

glib::wrapper! {
    pub struct LoaderModRow(ObjectSubclass<imp::LoaderModRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl LoaderModRow {
    pub const LOADER_MOD: &str = "loader-mod";
    pub const PAGE: &str = "page";

    pub fn new(loader_mod: &GLoaderMod, page: &FabricInstancePropertiesPage) -> Self {
        glib::Object::new(&[(Self::LOADER_MOD, loader_mod), (Self::PAGE, page)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property(GLoaderMod::NAME, &imp.name_label.get(), "label");
        self.bind_property(
            GLoaderMod::DESCRIPTION,
            &imp.description_label.get(),
            "label",
        );
        self.bind_property(GLoaderMod::ENABLED, &imp.enabled_switch.get(), "state");

        // Icon
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            if let Err(err) = this.load_icon().await {
                error_dialog(err);
            }
        }));
    }

    #[template_callback]
    async fn on_enabled_changed(&self) {
        let imp = self.imp();

        let mut loader_mod = LoaderMod::from(self.loader_mod());

        let result: anyhow::Result<LoaderMod> = match imp.enabled_switch.state() {
            true => {
                // Enable
                spawn_tokio_future(async move {
                    loader_mod.enable().await?;
                    Ok(loader_mod)
                })
                .await
            }
            false => {
                // Disable
                spawn_tokio_future(async move {
                    loader_mod.disable().await?;
                    Ok(loader_mod)
                })
                .await
            }
        };

        match result {
            Ok(loader_mod) => self.loader_mod().set_path(loader_mod.path),
            Err(err) => error_dialog(err),
        }
    }

    #[template_callback]
    fn on_ask_remove(&self) {
        let parent = self.page().window().upgrade();
        let dialog = adw::MessageDialog::new(
            parent.as_ref(),
            Some(&gettext("Remove Loader Mod")),
            Some(&gettext("This removes the file from the disk!")),
        );

        dialog.add_response("remove", &gettext("Remove"));
        dialog.add_response("cancel", &gettext("Cancel"));

        dialog.show();

        dialog.connect_response(
            None,
            clone!(@weak self as this => move |dialog, response| {
                if response == "remove" {
                    this.remove();
                }

                dialog.close();
            }),
        );
    }

    fn remove(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            this.page().remove_loader_mod(this.loader_mod()).await;
        }));
    }

    async fn load_icon(&self) -> anyhow::Result<()> {
        let loader_mod = LoaderMod::from(self.loader_mod());
        let icon_path =
            spawn_tokio_future(async move { load_and_cache_icon(&loader_mod).await }).await?;

        let pixbuf = match icon_path {
            Some(path) => {
                let file = gio::File::for_path(&path);
                let stream = file.read_future(Priority::default()).await?;
                Pixbuf::from_stream_at_scale_future(&stream, 48, 48, true).await?
            }
            None => Pixbuf::from_resource_at_scale(
                "/com/gitlab/stefan99353/cobble/images/pack.png",
                48,
                48,
                true,
            )?,
        };

        self.imp().icon_image.set_from_pixbuf(Some(&pixbuf));

        Ok(())
    }

    fn loader_mod(&self) -> GLoaderMod {
        self.property(Self::LOADER_MOD)
    }

    fn page(&self) -> FabricInstancePropertiesPage {
        self.property(Self::PAGE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.loader_mod()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}

/// Needs a tokio runtime
async fn load_and_cache_icon(loader_mod: &LoaderMod) -> anyhow::Result<Option<PathBuf>> {
    let cached_name = format!(
        "{:x}.png",
        Sha1::digest(loader_mod.path.to_string_lossy().to_string())
    );

    let mut cached_path = paths::CACHE.clone();
    cached_path.push("loader_mod_icons");
    cached_path.push(cached_name);

    if cached_path.is_file() {
        return Ok(Some(cached_path));
    }

    if let Some(parent) = cached_path.parent() {
        create_dir_all(parent).await?;
    }

    let icon_bytes = match loader_mod.icon().await? {
        Some(bytes) => bytes,
        None => return Ok(None),
    };

    let mut file = tokio::fs::File::create(&cached_path).await?;
    file.write_all(&icon_bytes).await?;
    file.sync_all().await?;

    Ok(Some(cached_path))
}
