use super::{
    FabricInstancePropertiesPage, GameInstancePropertiesPage, GeneralInstancePropertiesPage,
    JavaInstancePropertiesPage, LogFilesInstancePropertiesPage,
    ResourcepacksInstancePropertiesPage, SaveGamesInstancePropertiesPage,
    ScreenshotsInstancePropertiesPage, ServersInstancePropertiesPage,
    ShaderpacksInstancePropertiesPage,
};
use crate::config;
use crate::gobject::{GInstance, GInstanceManager};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::Instance;
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/window.ui")]
    pub struct InstancePropertiesWindow {
        #[template_child]
        pub save_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub content_stack: TemplateChild<gtk::Stack>,

        #[template_child]
        pub general_page: TemplateChild<GeneralInstancePropertiesPage>,
        #[template_child]
        pub game_page: TemplateChild<GameInstancePropertiesPage>,
        #[template_child]
        pub java_page: TemplateChild<JavaInstancePropertiesPage>,
        #[template_child]
        pub fabric_page: TemplateChild<FabricInstancePropertiesPage>,
        #[template_child]
        pub save_games_page: TemplateChild<SaveGamesInstancePropertiesPage>,
        #[template_child]
        pub servers_page: TemplateChild<ServersInstancePropertiesPage>,
        #[template_child]
        pub shaderpacks_page: TemplateChild<ShaderpacksInstancePropertiesPage>,
        #[template_child]
        pub screenshots_page: TemplateChild<ScreenshotsInstancePropertiesPage>,
        #[template_child]
        pub resourcepacks_page: TemplateChild<ResourcepacksInstancePropertiesPage>,
        #[template_child]
        pub logs_page: TemplateChild<LogFilesInstancePropertiesPage>,

        pub instance: RefCell<GInstance>,
        pub old_instance: RefCell<GInstance>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for InstancePropertiesWindow {
        const NAME: &'static str = "InstancePropertiesWindow";
        type Type = super::InstancePropertiesWindow;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for InstancePropertiesWindow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::InstancePropertiesWindow::INSTANCE,
                    "Instance",
                    "Instance",
                    GInstance::static_type(),
                    ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::InstancePropertiesWindow::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::InstancePropertiesWindow::INSTANCE => self.instance.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for InstancePropertiesWindow {}
    impl WindowImpl for InstancePropertiesWindow {}
    impl AdwWindowImpl for InstancePropertiesWindow {}
}

glib::wrapper! {
    pub struct InstancePropertiesWindow(ObjectSubclass<imp::InstancePropertiesWindow>)
    @extends gtk::Widget, gtk::Window, adw::Window;
}

#[gtk::template_callbacks]
impl InstancePropertiesWindow {
    pub const INSTANCE: &str = "instance";

    pub fn new(instance: GInstance) -> Self {
        // Clone the instance (get rid of references)
        let old_instance = instance.clone();
        let instance = Instance::from(instance);
        let instance = GInstance::from(instance);

        let object: Self = glib::Object::new(&[(Self::INSTANCE, &instance)]);
        *object.imp().old_instance.borrow_mut() = old_instance;

        object
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        imp.fabric_page.set_window(self.downgrade());
        imp.java_page.set_window(self.downgrade());
        imp.resourcepacks_page.set_window(self.downgrade());
        imp.save_games_page.set_window(self.downgrade());
        imp.screenshots_page.set_window(self.downgrade());
        imp.servers_page.set_window(self.downgrade());
        imp.shaderpacks_page.set_window(self.downgrade());
    }

    #[template_callback]
    #[instrument(name = "on_content_page_changed", skip_all, fields(view))]
    fn on_content_page_changed(&self) {
        let imp = self.imp();

        let page_name = imp
            .content_stack
            .visible_child_name()
            .map(|s| s.to_string());
        tracing::Span::current().record("view", &page_name);

        if page_name.is_none() {
            return;
        }

        trace!("Stack view changed");
        let imp = self.imp();
        if let Ok(view) = View::try_from(page_name.unwrap()) {
            match view {
                View::General => {}
                View::Game => {}
                View::Java => {}
                View::Fabric => {
                    imp.fabric_page.load_manifest();
                    imp.fabric_page.load_loader_mods();
                }
                View::SaveGames => {
                    imp.save_games_page.load_save_games();
                }
                View::Servers => {
                    imp.servers_page.load_servers();
                }
                View::Shaderpacks => {
                    imp.shaderpacks_page.load_shaderpacks();
                }
                View::Screenshots => {
                    imp.screenshots_page.load_screenshots();
                }
                View::Resourcepacks => {
                    imp.resourcepacks_page.load_resourcepacks();
                }
                View::Logs => {
                    imp.logs_page.load_log_files();
                }
            }
        }
    }

    #[template_callback]
    fn on_valid_changed(&self) {
        let imp = self.imp();
        imp.save_button.set_sensitive(imp.general_page.valid());
    }

    #[template_callback]
    fn on_save_button_clicked(&self) {
        let instance_manager = GInstanceManager::default();

        let new_instance = self.instance();
        let old_instance = self.imp().old_instance.borrow();

        // Check if installed state needs to be changed
        // Version changed
        if new_instance.version() != old_instance.version() {
            new_instance.set_installed(false);
        }
        // Fabric version changed
        if new_instance.fabric_version() != old_instance.fabric_version()
            && new_instance.fabric_version().is_some()
        {
            new_instance.set_installed(false);
        }

        instance_manager.update_instance(new_instance);
        self.close();
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }
}

#[derive(Clone, Debug)]
enum View {
    General,
    Game,
    Java,
    Fabric,
    SaveGames,
    Servers,
    Shaderpacks,
    Screenshots,
    Resourcepacks,
    Logs,
}

impl TryFrom<String> for View {
    type Error = ();

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "general" => Ok(Self::General),
            "game" => Ok(Self::Game),
            "java" => Ok(Self::Java),
            "fabric" => Ok(Self::Fabric),
            "save-games" => Ok(Self::SaveGames),
            "servers" => Ok(Self::Servers),
            "shaderpacks" => Ok(Self::Shaderpacks),
            "screenshots" => Ok(Self::Screenshots),
            "resourcepacks" => Ok(Self::Resourcepacks),
            "logs" => Ok(Self::Logs),
            _ => Err(()),
        }
    }
}
