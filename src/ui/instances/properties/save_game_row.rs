use super::SaveGamesInstancePropertiesPage;
use crate::gobject::GSaveGame;
use crate::utils::error_dialog;
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::{Difficulty, GameType, SaveGame};
use gdk::gdk_pixbuf::Pixbuf;
use gettextrs::gettext;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, Priority};
use once_cell::sync::{Lazy, OnceCell};

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/save_game_row.ui"
    )]
    pub struct SaveGameRow {
        #[template_child]
        pub icon_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub difficulty_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub game_type_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub last_played_label: TemplateChild<gtk::Label>,

        pub save_game: OnceCell<GSaveGame>,
        pub page: OnceCell<SaveGamesInstancePropertiesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SaveGameRow {
        const NAME: &'static str = "SaveGameRow";
        type Type = super::SaveGameRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SaveGameRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::SaveGameRow::SAVE_GAME,
                        "Savegame",
                        "Savegame",
                        GSaveGame::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecObject::new(
                        super::SaveGameRow::PAGE,
                        "Page",
                        "Page",
                        SaveGamesInstancePropertiesPage::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::SaveGameRow::SAVE_GAME => self.save_game.set(value.get().unwrap()).unwrap(),
                super::SaveGameRow::PAGE => self.page.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::SaveGameRow::SAVE_GAME => self.save_game.get().to_value(),
                super::SaveGameRow::PAGE => self.page.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for SaveGameRow {}
    impl ListBoxRowImpl for SaveGameRow {}
}

glib::wrapper! {
    pub struct SaveGameRow(ObjectSubclass<imp::SaveGameRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl SaveGameRow {
    pub const SAVE_GAME: &str = "save-game";
    pub const PAGE: &str = "page";

    pub fn new(save_game: &GSaveGame, page: &SaveGamesInstancePropertiesPage) -> Self {
        glib::Object::new(&[(Self::SAVE_GAME, save_game), (Self::PAGE, page)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property(GSaveGame::NAME, &imp.name_label.get(), "label");

        // Difficulty
        if let Some(difficulty) = self.save_game().difficulty() {
            let label = match difficulty {
                Difficulty::Peaceful => gettext("Peaceful"),
                Difficulty::Easy => gettext("Easy"),
                Difficulty::Normal => gettext("Normal"),
                Difficulty::Hard => gettext("Hard"),
            };
            imp.difficulty_label.set_label(&label);
        }

        // GameType
        let game_type = match self.save_game().game_type() {
            GameType::Survival => gettext("Survival"),
            GameType::Creative => gettext("Creative"),
            GameType::Adventure => gettext("Adventure"),
            GameType::Spectator => gettext("Spectator"),
        };
        imp.game_type_label.set_label(&game_type);

        // LastPlayed
        let last_played = self
            .save_game()
            .last_played()
            .format(&time::format_description::well_known::Rfc2822)
            .unwrap();
        imp.last_played_label.set_label(&last_played);

        // Icon
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            if let Err(err) = this.load_icon().await {
                error_dialog(err);
            }
        }));
    }

    #[template_callback]
    fn on_ask_remove(&self) {
        let parent = self.page().window().upgrade();
        let dialog = adw::MessageDialog::new(
            parent.as_ref(),
            Some(&gettext("Remove Save Game")),
            Some(&gettext("This removes the save game from the disk!")),
        );

        dialog.add_response("remove", &gettext("Remove"));
        dialog.add_response("cancel", &gettext("Cancel"));

        dialog.show();

        dialog.connect_response(
            None,
            clone!(@weak self as this => move |dialog, response| {
                if response == "remove" {
                    this.remove();
                }

                dialog.close();
            }),
        );
    }

    fn remove(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            this.page().remove_save_game(this.save_game()).await;
        }));
    }

    async fn load_icon(&self) -> anyhow::Result<()> {
        let save_game = SaveGame::from(self.save_game());
        let path = save_game.icon_path();

        let pixbuf = match path.is_file() {
            true => {
                let file = gio::File::for_path(&path);
                let stream = file.read_future(Priority::default()).await?;
                Pixbuf::from_stream_at_scale_future(&stream, 48, 48, true).await?
            }
            false => Pixbuf::from_resource_at_scale(
                "/com/gitlab/stefan99353/cobble/images/pack.png",
                48,
                48,
                true,
            )?,
        };

        self.imp().icon_image.set_from_pixbuf(Some(&pixbuf));

        Ok(())
    }

    fn save_game(&self) -> GSaveGame {
        self.property(Self::SAVE_GAME)
    }

    fn page(&self) -> SaveGamesInstancePropertiesPage {
        self.property(Self::PAGE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.save_game()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}
