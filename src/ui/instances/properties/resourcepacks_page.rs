use super::{InstancePropertiesWindow, ResourcepackRow};
use crate::gobject::{GInstance, GResourcepack};
use crate::utils::{
    error_dialog, native_file_chooser_async, open_path, spawn_tokio_future, ListStoreExt,
};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::Resourcepack;
use cobble_core::Instance;
use gettextrs::gettext;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use gtk::{FileChooserAction, FileFilter};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/resourcepacks_page.ui"
    )]
    pub struct ResourcepacksInstancePropertiesPage {
        #[template_child]
        pub open_resourcepack_folder_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub resourcepacks_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub resourcepacks_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub resourcepacks: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ResourcepacksInstancePropertiesPage {
        const NAME: &'static str = "ResourcepacksInstancePropertiesPage";
        type Type = super::ResourcepacksInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ResourcepacksInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ResourcepacksInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::ResourcepacksInstancePropertiesPage::RESOURCEPACKS,
                        "Resourcepacks",
                        "Resourcepacks",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ResourcepacksInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::ResourcepacksInstancePropertiesPage::RESOURCEPACKS => {
                    *self.resourcepacks.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ResourcepacksInstancePropertiesPage::INSTANCE => {
                    self.instance.borrow().to_value()
                }
                super::ResourcepacksInstancePropertiesPage::RESOURCEPACKS => {
                    self.resourcepacks.borrow().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ResourcepacksInstancePropertiesPage {}
    impl PreferencesPageImpl for ResourcepacksInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct ResourcepacksInstancePropertiesPage(ObjectSubclass<imp::ResourcepacksInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl ResourcepacksInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const RESOURCEPACKS: &str = "resourcepacks";

    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        // Bind ListBox
        self.imp().resourcepacks_listbox.bind_model(
            Some(&self.resourcepacks()),
            clone!(@weak self as this => @default-panic, move |object| {
                let resourcepack = object.downcast_ref::<GResourcepack>().unwrap();
                ResourcepackRow::new(resourcepack, &this).upcast()
            }),
        );
    }

    #[template_callback]
    async fn on_add_resourcepack_clicked(&self) {
        let parent = self.imp().window.get().and_then(|w| w.upgrade());

        let filter = FileFilter::new();
        filter.add_mime_type("application/zip");
        filter.set_name(Some(&gettext("Archive (.zip)")));

        let path = native_file_chooser_async(
            Some(&gettext("Add Resourcepack")),
            parent.as_ref(),
            FileChooserAction::Open,
            Some(&gettext("Add")),
            Some(&gettext("Cancel")),
            &[filter],
            true,
        )
        .await;

        let instance = Instance::from(self.instance());
        if let Some(src) = path {
            let add_future =
                spawn_tokio_future(async move { instance.add_resourcepack(src).await });

            match add_future.await {
                Ok(Some(resourcepack)) => {
                    self.add_resourcepack(resourcepack);
                }
                Ok(None) => {
                    error_dialog(gettext("The selected file is not a valid resourcepack"));
                }
                Err(err) => {
                    error_dialog(err);
                }
            }
        }
    }

    #[template_callback]
    fn on_open_folder(&self) {
        let instance = Instance::from(self.instance());
        open_path(instance.resourcepacks_path());
    }

    pub fn load_resourcepacks(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let resourcepacks_result = spawn_tokio_future(async move {instance.load_resourcepacks().await}).await;
            match resourcepacks_result {
                Ok(resourcepacks) => {
                    this.set_resourcepacks(resourcepacks);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_resourcepacks(&self, mut resourcepacks: Vec<Resourcepack>) {
        // Sort resourcepacks by name
        resourcepacks.sort_unstable_by(|a, b| a.name.cmp(&b.name));

        // Set resourcepacks
        let resourcepacks = resourcepacks
            .into_iter()
            .map(GResourcepack::from)
            .collect::<Vec<_>>();
        self.resourcepacks()
            .splice(0, self.resourcepacks().n_items(), &resourcepacks);

        self.update_view();
    }

    fn add_resourcepack(&self, resourcepack: Resourcepack) {
        let resourcepack = GResourcepack::from(resourcepack);
        self.resourcepacks().append(&resourcepack);

        self.update_view();
    }

    pub async fn remove_resourcepack(&self, resourcepack: GResourcepack) {
        let index = self
            .resourcepacks()
            .index_of::<_, GResourcepack>(|x| resourcepack.path() == x.path());
        match index {
            Some(i) => {
                let resourcepack = Resourcepack::from(resourcepack);
                let result = spawn_tokio_future(resourcepack.remove()).await;

                match result {
                    Ok(_) => self.resourcepacks().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown resourcepack"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.resourcepacks().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::Resourcepacks),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.resourcepacks_stack
            .set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn resourcepacks(&self) -> ListStore {
        self.property(Self::RESOURCEPACKS)
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    Resourcepacks,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Resourcepacks => "resourcepacks",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
