use super::{InstancePropertiesWindow, LoaderModRow};
use crate::gobject::{GFabricLoaderInfo, GInstance, GLoaderMod};
use crate::utils::ListStoreExt;
use crate::utils::{error_dialog, native_file_chooser_async, open_path, spawn_tokio_future};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::FabricLoaderSummary;
use cobble_core::{minecraft::LoaderMod, Instance};
use gettextrs::gettext;
use gio::ListStore;
use glib::{clone, BindingFlags, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use gtk::{FileChooserAction, FileFilter, SignalListItemFactory, SingleSelection};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/fabric_page.ui")]
    pub struct FabricInstancePropertiesPage {
        #[template_child]
        pub use_fabric_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub version_listview: TemplateChild<gtk::ListView>,
        #[template_child]
        pub open_mods_folder_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub add_mod_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub mods_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub mods_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub versions: RefCell<ListStore>,
        pub version_selection: SingleSelection,
        pub loader_mods: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FabricInstancePropertiesPage {
        const NAME: &'static str = "FabricInstancePropertiesPage";
        type Type = super::FabricInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FabricInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::FabricInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::FabricInstancePropertiesPage::VERSIONS,
                        "Versions",
                        "Versions",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::FabricInstancePropertiesPage::LOADER_MODS,
                        "Loader Mods",
                        "Loader Mods",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::FabricInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::FabricInstancePropertiesPage::VERSIONS => {
                    *self.versions.borrow_mut() = value.get().unwrap()
                }
                super::FabricInstancePropertiesPage::LOADER_MODS => {
                    *self.loader_mods.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::FabricInstancePropertiesPage::INSTANCE => self.instance.borrow().to_value(),
                super::FabricInstancePropertiesPage::VERSIONS => self.versions.borrow().to_value(),
                super::FabricInstancePropertiesPage::LOADER_MODS => {
                    self.loader_mods.borrow().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
            obj.setup_signals();
        }
    }

    impl WidgetImpl for FabricInstancePropertiesPage {}
    impl PreferencesPageImpl for FabricInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct FabricInstancePropertiesPage(ObjectSubclass<imp::FabricInstancePropertiesPage>)
    @extends gtk::Widget, gtk::Window, gtk::Dialog;
}

#[gtk::template_callbacks]
impl FabricInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const VERSIONS: &str = "versions";
    pub const LOADER_MODS: &str = "loader-mods";

    fn setup_widgets(&self) {
        let imp = self.imp();
        self.set_view(View::Loading);

        // Versions
        imp.version_selection.set_autoselect(false);
        imp.version_selection.set_model(Some(&self.versions()));
        imp.version_listview
            .set_factory(Some(&fabric_loader_list_factory()));
        imp.version_listview.set_model(Some(&imp.version_selection));

        // Bind ListBox
        imp.mods_listbox.bind_model(
            Some(&self.loader_mods()),
            clone!(@weak self as this => @default-panic, move |object| {
                let loader_mod = object.downcast_ref::<GLoaderMod>().unwrap();
                LoaderModRow::new(loader_mod, &this).upcast()
            }),
        );
    }

    fn setup_signals(&self) {
        let imp = self.imp();

        self.connect_notify_local(
            Some(Self::INSTANCE),
            clone!(@weak self as this => move |_, _| {
                this.set_bindings();
            }),
        );

        imp.version_selection.connect_selected_item_notify(
            clone!(@weak self as this => move |selection| {
                match selection.selected_item() {
                    Some(item) => {
                        let version = item.downcast::<GFabricLoaderInfo>().unwrap();
                        this.instance().set_fabric_version(Some(version.version()));
                    },
                    None => this.instance().set_fabric_version(None),
                }
            }),
        );
    }

    #[template_callback]
    async fn on_add_mod_clicked(&self) {
        let parent = self.imp().window.get().and_then(|w| w.upgrade());

        let filter = FileFilter::new();
        filter.add_mime_type("application/java-archive");
        filter.set_name(Some(&gettext("Java Archive (.jar)")));

        let path = native_file_chooser_async(
            Some(&gettext("Add Loader Mod")),
            parent.as_ref(),
            FileChooserAction::Open,
            Some(&gettext("Add")),
            Some(&gettext("Cancel")),
            &[filter],
            true,
        )
        .await;

        let instance = Instance::from(self.instance());
        if let Some(src) = path {
            let add_future = spawn_tokio_future(async move { instance.add_loader_mod(src).await });

            match add_future.await {
                Ok(Some(loader_mod)) => {
                    self.add_loader_mod(loader_mod);
                }
                Ok(None) => {
                    error_dialog(gettext("The selected file is not a valid loader mod"));
                }
                Err(err) => {
                    error_dialog(err);
                }
            }
        }
    }

    #[template_callback]
    fn on_open_folder(&self) {
        let instance = Instance::from(self.instance());
        let path = instance.loader_mods_path();
        open_path(path);
    }

    fn set_bindings(&self) {
        let imp = self.imp();

        imp.use_fabric_expander
            .set_enable_expansion(self.instance().fabric_version().is_some());

        self.bind_property(
            "fabric-version",
            &imp.use_fabric_expander.get(),
            "subtitle",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
    }

    pub fn load_manifest(&self) {
        let game_version = self.instance().version();

        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let fetch_future = spawn_tokio_future(async move {
                FabricLoaderSummary::fetch_manifest(&game_version).await
            });

            match fetch_future.await {
                Ok(manifest) => this.populate_version_list(manifest),
                Err(err) => {
                    error_dialog(err)
                }
            }
        }));
    }

    fn populate_version_list(&self, manifest: Vec<FabricLoaderSummary>) {
        let imp = self.imp();

        // Parse versions
        let versions = manifest
            .into_iter()
            .map(|v| GFabricLoaderInfo::from(v.loader))
            .collect::<Vec<_>>();

        self.versions()
            .splice(0, self.versions().n_items(), &versions);

        // Select current version
        if let Some(fabric_version) = self.instance().fabric_version() {
            let versions = self.versions();
            for pos in 0..versions.n_items() {
                let temp_version = versions
                    .item(pos)
                    .unwrap()
                    .downcast::<GFabricLoaderInfo>()
                    .unwrap();

                if fabric_version == temp_version.version() {
                    imp.version_selection.set_selected(pos);
                    break;
                }
            }
        }
    }

    pub fn load_loader_mods(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let loader_mods_result = spawn_tokio_future(async move {instance.load_loader_mods().await}).await;
            match loader_mods_result {
                Ok(loader_mods) => {
                    this.set_loader_mods(loader_mods);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_loader_mods(&self, mut loader_mods: Vec<LoaderMod>) {
        // Sort loader mods by name
        loader_mods.sort_unstable_by(|a, b| a.name.cmp(&b.name));

        // Set resourcepacks
        let loader_mods = loader_mods
            .into_iter()
            .map(GLoaderMod::from)
            .collect::<Vec<_>>();
        self.loader_mods()
            .splice(0, self.loader_mods().n_items(), &loader_mods);

        self.update_view();
    }

    fn add_loader_mod(&self, loader_mod: LoaderMod) {
        let loader_mod = GLoaderMod::from(loader_mod);
        self.loader_mods().append(&loader_mod);

        self.update_view();
    }

    pub async fn remove_loader_mod(&self, loader_mod: GLoaderMod) {
        let index = self
            .loader_mods()
            .index_of::<_, GLoaderMod>(|x| loader_mod.path() == x.path());
        match index {
            Some(i) => {
                let loader_mod = LoaderMod::from(loader_mod);
                let result = spawn_tokio_future(loader_mod.remove()).await;

                match result {
                    Ok(_) => self.loader_mods().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown loader mod"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.loader_mods().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::LoaderMods),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.mods_stack.set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn versions(&self) -> ListStore {
        self.property(Self::VERSIONS)
    }

    fn loader_mods(&self) -> ListStore {
        self.property(Self::LOADER_MODS)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
        flags: BindingFlags,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(flags)
            .build();
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    LoaderMods,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::LoaderMods => "loader-mods",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}

fn fabric_loader_list_factory() -> SignalListItemFactory {
    let factory = SignalListItemFactory::new();

    factory.connect_bind(move |_, item| {
        let item = item.clone().downcast::<gtk::ListItem>().unwrap();
        let version = item
            .item()
            .unwrap()
            .downcast::<GFabricLoaderInfo>()
            .unwrap();

        let label = gtk::Label::new(Some(&version.version()));
        label.set_valign(gtk::Align::Center);
        label.set_halign(gtk::Align::Start);
        item.set_child(Some(&label));
    });

    factory
}
