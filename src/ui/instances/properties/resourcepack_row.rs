use super::ResourcepacksInstancePropertiesPage;
use crate::gobject::GResourcepack;
use crate::paths;
use crate::utils::error_dialog;
use crate::utils::spawn_tokio_future;
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::Resourcepack;
use gdk::gdk_pixbuf::Pixbuf;
use gettextrs::gettext;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, Priority};
use once_cell::sync::{Lazy, OnceCell};
use sha1::{Digest, Sha1};
use std::path::PathBuf;
use tokio::fs::create_dir_all;
use tokio::io::AsyncWriteExt;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/resourcepack_row.ui"
    )]
    pub struct ResourcepackRow {
        #[template_child]
        pub icon_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub path_label: TemplateChild<gtk::Label>,

        pub resourcepack: OnceCell<GResourcepack>,
        pub page: OnceCell<ResourcepacksInstancePropertiesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ResourcepackRow {
        const NAME: &'static str = "ResourcepackRow";
        type Type = super::ResourcepackRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ResourcepackRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ResourcepackRow::RESOURCEPACK,
                        "Resourcepack",
                        "Resourcepack",
                        GResourcepack::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecObject::new(
                        super::ResourcepackRow::PAGE,
                        "Page",
                        "Page",
                        ResourcepacksInstancePropertiesPage::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ResourcepackRow::RESOURCEPACK => {
                    self.resourcepack.set(value.get().unwrap()).unwrap()
                }
                super::ResourcepackRow::PAGE => self.page.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ResourcepackRow::RESOURCEPACK => self.resourcepack.get().to_value(),
                super::ResourcepackRow::PAGE => self.page.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ResourcepackRow {}
    impl ListBoxRowImpl for ResourcepackRow {}
}

glib::wrapper! {
    pub struct ResourcepackRow(ObjectSubclass<imp::ResourcepackRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl ResourcepackRow {
    pub const RESOURCEPACK: &str = "resourcepack";
    pub const PAGE: &str = "page";

    pub fn new(resourcepack: &GResourcepack, page: &ResourcepacksInstancePropertiesPage) -> Self {
        glib::Object::new(&[(Self::RESOURCEPACK, resourcepack), (Self::PAGE, page)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property("name", &imp.name_label.get(), "label");
        self.bind_property("path", &imp.path_label.get(), "label");

        // Icon
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            if let Err(err) = this.load_icon().await {
                error_dialog(err);
            }
        }));
    }

    #[template_callback]
    fn on_ask_remove(&self) {
        let parent = self.page().window().upgrade();
        let dialog = adw::MessageDialog::new(
            parent.as_ref(),
            Some(&gettext("Remove Resourcepack")),
            Some(&gettext("This removes the file from the disk!")),
        );

        dialog.add_response("remove", &gettext("Remove"));
        dialog.add_response("cancel", &gettext("Cancel"));

        dialog.show();

        dialog.connect_response(
            None,
            clone!(@weak self as this => move |dialog, response| {
                if response == "remove" {
                    this.remove();
                }

                dialog.close();
            }),
        );
    }

    fn remove(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            this.page().remove_resourcepack(this.resourcepack()).await;
        }));
    }

    async fn load_icon(&self) -> anyhow::Result<()> {
        let resourcepack = Resourcepack::from(self.resourcepack());
        let icon_path =
            spawn_tokio_future(async move { load_and_cache_icon(&resourcepack).await }).await?;

        let pixbuf = match icon_path {
            Some(path) => {
                let file = gio::File::for_path(&path);
                let stream = file.read_future(Priority::default()).await?;
                Pixbuf::from_stream_at_scale_future(&stream, 48, 48, true).await?
            }
            None => Pixbuf::from_resource_at_scale(
                "/com/gitlab/stefan99353/cobble/images/pack.png",
                48,
                48,
                true,
            )?,
        };

        self.imp().icon_image.set_from_pixbuf(Some(&pixbuf));

        Ok(())
    }

    fn resourcepack(&self) -> GResourcepack {
        self.property(Self::RESOURCEPACK)
    }

    fn page(&self) -> ResourcepacksInstancePropertiesPage {
        self.property(Self::PAGE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.resourcepack()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}

/// Needs a tokio runtime
async fn load_and_cache_icon(resourcepack: &Resourcepack) -> anyhow::Result<Option<PathBuf>> {
    let cached_name = format!(
        "{:x}.png",
        Sha1::digest(resourcepack.path.to_string_lossy().to_string())
    );

    let mut cached_path = paths::CACHE.clone();
    cached_path.push("resourcepack_icons");
    cached_path.push(cached_name);

    if cached_path.is_file() {
        return Ok(Some(cached_path));
    }

    if let Some(parent) = cached_path.parent() {
        create_dir_all(parent).await?;
    }

    let icon_bytes = match resourcepack.icon().await? {
        Some(bytes) => bytes,
        None => return Ok(None),
    };

    let mut file = tokio::fs::File::create(&cached_path).await?;
    file.write_all(&icon_bytes).await?;
    file.sync_all().await?;

    Ok(Some(cached_path))
}
