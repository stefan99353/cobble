use super::VersionSummaryRow;
use crate::config;
use crate::gobject::{GInstance, GInstanceManager, GVersionSummary};
use crate::settings::SettingsKey;
use crate::utils::{conditional_style, error_dialog, spawn_tokio_future};
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::traits::ExpanderRowExt;
use cobble_core::minecraft::models::{VersionManifest, VersionType};
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject};
use gtk::{SignalListItemFactory, SingleSelection};
use itertools::Itertools;
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::path::PathBuf;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/new_instance_window.ui")]
    pub struct NewInstanceWindow {
        #[template_child]
        pub add_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub name_entryrow: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub description_entryrow: TemplateChild<adw::EntryRow>,

        #[template_child]
        pub version_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub version_listview: TemplateChild<gtk::ListView>,
        #[template_child]
        pub releases_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub snapshots_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub betas_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub alphas_switch: TemplateChild<gtk::Switch>,

        #[template_child]
        pub instance_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub instance_path_label: TemplateChild<gtk::Label>,

        pub instance: RefCell<GInstance>,

        pub all_versions: RefCell<Vec<GVersionSummary>>,
        pub versions: ListStore,
        pub version_selection: SingleSelection,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NewInstanceWindow {
        const NAME: &'static str = "NewInstanceWindow";
        type Type = super::NewInstanceWindow;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for NewInstanceWindow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::NewInstanceWindow::INSTANCE,
                    "Instance",
                    "Instance",
                    GInstance::static_type(),
                    ParamFlags::READABLE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::NewInstanceWindow::INSTANCE => self.instance.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.setup_widgets();
            obj.setup_signals();

            obj.load_manifest();
        }
    }

    impl WidgetImpl for NewInstanceWindow {}
    impl WindowImpl for NewInstanceWindow {}
    impl AdwWindowImpl for NewInstanceWindow {}
}

glib::wrapper! {
    pub struct NewInstanceWindow(ObjectSubclass<imp::NewInstanceWindow>)
    @extends gtk::Widget, gtk::Window, adw::Window;
}

#[gtk::template_callbacks]
impl NewInstanceWindow {
    pub const INSTANCE: &str = "instance";

    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new(&[])
    }

    #[template_callback]
    fn on_add_button_clicked(&self) {
        let instance_manager = GInstanceManager::default();

        let instance = self.instance();

        instance.set_libraries_path(PathBuf::from(
            SettingsKey::DefaultLibrariesPath.get_string(),
        ));
        instance.set_assets_path(PathBuf::from(SettingsKey::DefaultAssetsPath.get_string()));
        instance.set_fullscreen(SettingsKey::EnableGlobalFullscreen.get_bool());

        instance_manager.add_instance(instance);
        self.close();
    }

    #[template_callback]
    fn on_validate_form(&self) {
        let imp = self.imp();

        let name = imp.name_entryrow.text();
        let version = imp.version_expander.subtitle();

        conditional_style(name.is_empty(), &imp.name_entryrow.get(), "error");
        conditional_style(version.is_empty(), &imp.version_expander.get(), "error");

        let valid = !name.is_empty() && !version.is_empty();
        imp.add_button.set_sensitive(valid);

        // Set path
        let mut instance_path = PathBuf::from(SettingsKey::DefaultInstancesPath.get_string());
        if name.is_empty() {
            instance_path.push(self.instance().uuid().to_string());
        } else {
            instance_path.push(name.to_string().replace(' ', "_").to_ascii_lowercase());
        }
        imp.instance_path_label
            .set_label(&instance_path.to_string_lossy());
    }

    #[template_callback]
    fn on_version_filter_changed(&self) {
        self.populate_version_list();
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        imp.version_selection.set_autoselect(false);
        imp.version_selection.set_model(Some(&imp.versions));
        imp.version_listview
            .set_factory(Some(&version_summary_row_factory()));
        imp.version_listview.set_model(Some(&imp.version_selection));

        self.bind_property("name", &imp.name_entryrow.get(), "text");
        self.bind_property("description", &imp.description_entryrow.get(), "text");
        self.bind_property("version", &imp.version_expander.get(), "subtitle");
        self.bind_property("instance-path", &imp.instance_path_label.get(), "label");

        let mut instance_path = PathBuf::from(SettingsKey::DefaultInstancesPath.get_string());
        instance_path.push(self.instance().uuid().to_string());
        imp.instance_path_label
            .set_label(&instance_path.to_string_lossy());
    }

    fn setup_signals(&self) {
        let imp = self.imp();

        imp.version_selection.connect_selected_item_notify(
            clone!(@weak self as this => move |selection| {
                let span = info_span!("selected_version_changed", selected = tracing::field::Empty);
                let _entered = span.enter();
                let imp = this.imp();

                trace!("Getting selected instance");
                match selection.selected_item() {
                    Some(item) => {
                        let summary = item.downcast::<GVersionSummary>().unwrap();
                        span.record("selected", summary.id());

                        trace!("Displaying selected version");
                        imp.version_expander.set_subtitle(&summary.id());
                    }
                    None => {
                        trace!("Removing selected version from UI");
                        imp.version_expander.set_subtitle("");
                    }
                }
            }),
        );
    }

    #[instrument(name = "load_manifest", skip_all)]
    fn load_manifest(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let imp = this.imp();

            trace!("Fetching version manifest");
            match spawn_tokio_future(VersionManifest::fetch()).await {
                Ok(manifest) => {
                    *imp.all_versions.borrow_mut() = manifest.versions.into_iter()
                        .sorted_by(|(_, a), (_, b)| b.release_time.cmp(&a.release_time))
                        .map(|(_, version)| GVersionSummary::from(version))
                        .collect::<Vec<_>>();

                    this.populate_version_list();
                }
                Err(err) => error_dialog(err),
            }
        }));
    }

    fn populate_version_list(&self) {
        let imp = self.imp();

        trace!("Applying version filters on manifest");
        let versions = imp
            .all_versions
            .borrow()
            .iter()
            .cloned()
            .filter(|version| self.filter_version(version))
            .collect::<Vec<_>>();

        trace!("Appending versions to list");
        imp.versions.splice(0, imp.versions.n_items(), &versions);

        match versions.is_empty() {
            true => {
                imp.version_selection.unselect_all();
            }
            false => imp.version_selection.set_selected(0),
        }
    }

    fn filter_version(&self, version: &GVersionSummary) -> bool {
        let imp = self.imp();

        let releases = imp.releases_switch.state();
        let snapshots = imp.snapshots_switch.state();
        let betas = imp.betas_switch.state();
        let alphas = imp.alphas_switch.state();

        matches!(version.type_(), VersionType::Release) && releases
            || matches!(version.type_(), VersionType::Snapshot) && snapshots
            || matches!(version.type_(), VersionType::OldBeta) && betas
            || matches!(version.type_(), VersionType::OldAlpha) && alphas
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
            .build();
    }
}

fn version_summary_row_factory() -> SignalListItemFactory {
    let factory = SignalListItemFactory::new();

    factory.connect_bind(move |_, item| {
        let item = item.clone().downcast::<gtk::ListItem>().unwrap();
        let summary = item.item().unwrap().downcast::<GVersionSummary>().unwrap();

        let label = VersionSummaryRow::new(&summary);
        item.set_child(Some(&label));
    });

    factory
}
