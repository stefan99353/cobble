use crate::config;
use crate::gobject::GInstanceManager;
use crate::ui::InstancePage;
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;

mod imp {

    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/content_box.ui")]
    pub struct ContentBox {
        #[template_child]
        pub content_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub instance_page: TemplateChild<InstancePage>,
        #[template_child]
        pub ready_status_page: TemplateChild<adw::StatusPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContentBox {
        const NAME: &'static str = "ContentBox";
        type Type = super::ContentBox;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContentBox {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
            obj.setup_signals();
        }
    }

    impl WidgetImpl for ContentBox {}
    impl BoxImpl for ContentBox {}
}

glib::wrapper! {
    pub struct ContentBox(ObjectSubclass<imp::ContentBox>)
    @extends gtk::Widget, gtk::Box;
}

impl ContentBox {
    fn setup_widgets(&self) {
        let imp = self.imp();

        self.set_view(View::Loading);

        // Set Icon
        imp.ready_status_page.set_icon_name(Some(config::APP_ID));
    }

    fn setup_signals(&self) {
        let instance_manager = GInstanceManager::default();
        instance_manager.instances().connect_items_changed(
            clone!(@weak self as this => move |list, _, _, _| {
                trace!("Instances changed");

                match list.n_items() {
                    0 => this.set_view(View::Ready),
                    _ => this.set_view(View::Instances),
                };
            }),
        );
    }

    #[instrument(name = "set_view", skip_all, fields(view=view.get_name()))]
    fn set_view(&self, view: View) {
        trace!("Setting stack visible child name");
        let imp = self.imp();
        imp.content_stack.set_visible_child_name(view.get_name());
    }
}

#[derive(Clone, Debug)]
enum View {
    Instances,
    Ready,
    Loading,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Instances => "instances",
            View::Ready => "ready",
            View::Loading => "loading",
        }
    }
}
