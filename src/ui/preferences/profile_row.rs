use crate::gobject::{GCobbleProfile, GProfileManager};
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use once_cell::sync::{Lazy, OnceCell};

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/preferences/profile_row.ui")]
    pub struct ProfileRow {
        #[template_child]
        pub skin_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub remove_button: TemplateChild<gtk::Button>,

        pub profile: OnceCell<GCobbleProfile>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ProfileRow {
        const NAME: &'static str = "ProfileRow";
        type Type = super::ProfileRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ProfileRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::ProfileRow::PROFILE,
                    "Profile",
                    "Profile",
                    GCobbleProfile::static_type(),
                    ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ProfileRow::PROFILE => self.profile.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ProfileRow::PROFILE => self.profile.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ProfileRow {}
    impl ListBoxRowImpl for ProfileRow {}
}

glib::wrapper! {
    pub struct ProfileRow(ObjectSubclass<imp::ProfileRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl ProfileRow {
    pub const PROFILE: &str = "profile";

    pub fn new(profile: &GCobbleProfile) -> Self {
        glib::Object::new(&[(Self::PROFILE, profile)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property("player-name", &imp.name_label.get(), "label");
    }

    #[template_callback]
    #[instrument(name = "on_remove_clicked", skip_all, fields(uuid))]
    fn on_remove_clicked(&self) {
        let uuid = self.profile().uuid();
        tracing::Span::current().record("uuid", tracing::field::display(uuid));

        debug!("Removing profile");
        let profile_manager = GProfileManager::default();
        profile_manager.remove_profile(uuid);
    }

    fn profile(&self) -> GCobbleProfile {
        self.property(Self::PROFILE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.profile()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}
