use super::{PreferencesWindow, ProfileRow};
use crate::gobject::{GCobbleProfile, GProfileManager};
use crate::ui::NewProfileWindow;
use adw::prelude::*;
use adw::subclass::prelude::PreferencesPageImpl;
use adw::subclass::prelude::*;
use glib::{clone, WeakRef};
use once_cell::sync::OnceCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/preferences/profiles_page.ui")]
    pub struct ProfilesPage {
        #[template_child]
        pub add_profile_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub profiles_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub profiles_listbox: TemplateChild<gtk::ListBox>,

        pub window: OnceCell<WeakRef<PreferencesWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ProfilesPage {
        const NAME: &'static str = "ProfilesPage";
        type Type = super::ProfilesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ProfilesPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
            obj.setup_signals();
        }
    }

    impl WidgetImpl for ProfilesPage {}
    impl PreferencesPageImpl for ProfilesPage {}
}

glib::wrapper! {
    pub struct ProfilesPage(ObjectSubclass<imp::ProfilesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl ProfilesPage {
    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        let imp = self.imp();
        let profile_manager = GProfileManager::default();

        imp.profiles_listbox
            .bind_model(Some(&profile_manager.profiles()), |object| {
                let profile = object.downcast_ref::<GCobbleProfile>().unwrap();
                ProfileRow::new(profile).upcast()
            });
    }

    fn setup_signals(&self) {
        let profile_manager = GProfileManager::default();
        profile_manager.profiles().connect_items_changed(
            clone!(@weak self as this => move |list, _, _, _| {
                trace!("Profiles changed");

                match list.n_items() {
                    0 => this.set_view(View::Empty),
                    _ => this.set_view(View::Profiles),
                }
            }),
        );
    }

    #[template_callback]
    #[instrument(name = "on_add_profile_clicked", skip_all)]
    fn on_add_profile_clicked(&self) {
        trace!("Creating window for adding a profile");
        let window = self.imp().window.get().and_then(|x| x.upgrade());
        let new_profile_dialog = NewProfileWindow::default();
        new_profile_dialog.set_transient_for(window.as_ref());
        new_profile_dialog.set_modal(true);

        trace!("Showing window");
        new_profile_dialog.show();
    }

    #[instrument(name = "set_view", skip_all, fields(view=view.get_name()))]
    fn set_view(&self, view: View) {
        trace!("Setting stack visible child name");
        let imp = self.imp();
        imp.profiles_stack.set_visible_child_name(view.get_name());
    }

    pub fn set_window(&self, window: WeakRef<PreferencesWindow>) {
        let imp = self.imp();

        imp.window.set(window).unwrap();
    }
}

#[derive(Clone, Debug)]
enum View {
    Profiles,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Profiles => "profiles",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
