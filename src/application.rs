use crate::config;
use crate::gobject::{GInstanceManager, GProfileManager};
use crate::ui::Window;
use crate::ui::{NewInstanceWindow, NewProfileWindow, PreferencesWindow};
use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use glib::ParamFlags;
use glib::ParamSpec;
use glib::ParamSpecObject;
use glib::{clone, WeakRef};
use gtk_macros::action;
use once_cell::sync::Lazy;
use once_cell::sync::OnceCell;

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct Application {
        pub window: OnceCell<WeakRef<Window>>,

        pub profile_manager: GProfileManager,
        pub instance_manager: GInstanceManager,
    }

    impl Default for Application {
        fn default() -> Self {
            Self {
                window: OnceCell::default(),
                profile_manager: GProfileManager::new(),
                instance_manager: GInstanceManager::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for Application {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::Application::PROFILE_MANAGER,
                        "Profile Manager",
                        "Profile Manager",
                        GProfileManager::static_type(),
                        ParamFlags::READABLE,
                    ),
                    ParamSpecObject::new(
                        super::Application::INSTANCE_MANAGER,
                        "Instance Manager",
                        "Instance Manager",
                        GInstanceManager::static_type(),
                        ParamFlags::READABLE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::Application::PROFILE_MANAGER => self.profile_manager.to_value(),
                super::Application::INSTANCE_MANAGER => self.instance_manager.to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }

    impl ApplicationImpl for Application {
        fn activate(&self) {
            self.parent_activate();
            let application = self.instance();

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.present();
                return;
            }

            let window = Window::new(&application);
            self.window.set(window.downgrade()).unwrap();

            application.main_window().present();
        }

        fn startup(&self) {
            self.parent_startup();
            let application = self.instance();

            // Set icon for shell
            gtk::Window::set_default_icon_name(config::APP_ID);

            application.setup_css();
            application.setup_gactions();
            application.setup_accels();
        }
    }

    impl GtkApplicationImpl for Application {}

    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
    @extends gio::Application, gtk::Application, adw::Application,
    @implements gio::ActionMap, gio::ActionGroup;
}

impl Application {
    pub const PROFILE_MANAGER: &str = "profile-manager";
    pub const INSTANCE_MANAGER: &str = "instance-manager";

    pub fn new() -> Self {
        glib::Object::new::<Self>(&[
            ("application-id", &config::APP_ID),
            ("flags", &gio::ApplicationFlags::empty()),
            ("resource-base-path", &"/com/gitlab/stefan99353/cobble/"),
        ])
    }

    pub fn main_window(&self) -> Window {
        self.imp().window.get().unwrap().upgrade().unwrap()
    }

    fn setup_gactions(&self) {
        trace!("Setting up actions for application");

        trace!("Adding `app.add-instance`");
        action!(
            self,
            "add-instance",
            clone!(@weak self as this => move |_, _| this.add_instance())
        );

        trace!("Adding `app.add-profile`");
        action!(
            self,
            "add-profile",
            clone!(@weak self as this => move |_, _| this.add_profile())
        );

        trace!("Adding `app.preferences`");
        action!(
            self,
            "preferences",
            clone!(@weak self as this => move |_, _| this.preferences())
        );

        trace!("Adding `app.quit`");
        action!(
            self,
            "quit",
            clone!(@weak self as this => move |_, _| this.quit_action())
        );

        trace!("Adding `app.about`");
        action!(
            self,
            "about",
            clone!(@weak self as this => move |_, _| this.about())
        );
    }

    fn setup_accels(&self) {
        self.set_accels_for_action("app.add-instance", &["<primary>plus"]);
        self.set_accels_for_action("app.preferences", &["<primary>comma"]);
        self.set_accels_for_action("app.quit", &["<primary>q"]);
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("com/gitlab/stefan99353/cobble/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    #[instrument(name = "app.add-instance", skip_all)]
    fn add_instance(&self) {
        trace!("Creating window for adding a new instance");
        let main_window = self.active_window();
        let new_instance_dialog = NewInstanceWindow::new();
        new_instance_dialog.set_transient_for(main_window.as_ref());
        new_instance_dialog.set_modal(true);

        trace!("Showing window");
        new_instance_dialog.show();
    }

    #[instrument(name = "app.add-profile", skip_all)]
    fn add_profile(&self) {
        trace!("Creating window for adding a new profile");
        let main_window = self.active_window();
        let new_profile_dialog = NewProfileWindow::default();
        new_profile_dialog.set_transient_for(main_window.as_ref());
        new_profile_dialog.set_modal(true);

        trace!("Showing window");
        new_profile_dialog.show();
    }

    #[instrument(name = "app.preferences", skip_all)]
    fn preferences(&self) {
        trace!("Creating preferences window");
        let main_window = self.active_window();
        let preferences_window = PreferencesWindow::default();
        preferences_window.set_transient_for(main_window.as_ref());
        preferences_window.set_modal(true);

        trace!("Showing window");
        preferences_window.show();
    }

    #[instrument(name = "app.quit", skip_all)]
    fn quit_action(&self) {
        trace!("Closing main window");
        self.main_window().close();
        trace!("Quitting application");
        self.quit();
    }

    #[instrument(
        name = "app.about",
        level = "debug"
        skip_all,
        fields (
            application_name = &gettext("Cobble"),
            application_id = config::APP_ID,
            version = config::VERSION,
        )
    )]
    fn about(&self) {
        trace!("Building about window");
        let dialog = adw::AboutWindow::builder()
            .application_name(&gettext("Cobble"))
            .application_icon(config::APP_ID)
            .comments(&gettext("Minecraft Launcher for the Gnome Desktop"))
            .developer_name("Stefan Rupertsberger")
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.com/stefan99353/cobble/")
            .issue_url("https://gitlab.com/stefan99353/cobble/-/issues")
            .version(config::VERSION)
            .transient_for(&self.main_window())
            .modal(true)
            .copyright(&gettext("© 2022 Stefan Rupertsberger"))
            .developers(vec!["Stefan Rupertsberger".to_string()])
            .designers(vec!["Stefan Rupertsberger".to_string()])
            .translator_credits(&gettext("translator-credits"))
            .build();

        trace!("Showing window");
        dialog.show();
    }

    pub fn profile_mamanger(&self) -> GProfileManager {
        self.property(Self::PROFILE_MANAGER)
    }

    pub fn instance_manager(&self) -> GInstanceManager {
        self.property(Self::INSTANCE_MANAGER)
    }

    #[instrument(skip_all, fields(msg = msg))]
    pub fn toast_notification(&self, msg: &str) {
        let window = self.main_window();
        let window_imp = window.imp();

        debug!("Displaying toast message");
        let toast = adw::Toast::new(msg);
        window_imp.toast_overlay.add_toast(&toast);
    }

    pub fn run(&self) {
        info!("{} ({})", &gettext("Cobble"), config::APP_ID);
        debug!("Version: {} ({})", config::VERSION, config::PROFILE);
        debug!("Datadir: {}", config::PKGDATADIR);

        ApplicationExtManual::run(self);
    }
}

impl Default for Application {
    fn default() -> Self {
        gio::Application::default()
            .expect("Could not get default application")
            .downcast()
            .unwrap()
    }
}
