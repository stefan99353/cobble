use crate::config;
use gdk::prelude::{SettingsExt, SettingsExtManual};
use glib::IsA;

#[derive(Clone, Debug)]
pub enum SettingsKey {
    EnableGlobalFullscreen,
    EnableGlobalWindowSize,
    GlobalWindowWidth,
    GlobalWindowHeight,
    EnableGlobalMemory,
    GlobalMinMemory,
    GlobalMaxMemory,
    GlobalJavaExec,
    EnableGlobalJvmArgs,
    GlobalJvmArgs,
    DefaultInstancesPath,
    DefaultLibrariesPath,
    DefaultAssetsPath,
    ProfileIndexPath,
    InstanceIndexPath,
    ParallelDownloads,
    DownloadRetries,
    VerifyDownloads,
    DefaultProfileUuid,
    WindowWidth,
    WindowHeight,
    WindowMaximized,
}

#[allow(dead_code)]
impl SettingsKey {
    pub fn name(&self) -> &'static str {
        match self {
            SettingsKey::EnableGlobalFullscreen => "enable-global-fullscreen",
            SettingsKey::EnableGlobalWindowSize => "enable-global-window-size",
            SettingsKey::GlobalWindowWidth => "global-window-width",
            SettingsKey::GlobalWindowHeight => "global-window-height",
            SettingsKey::EnableGlobalMemory => "enable-global-memory",
            SettingsKey::GlobalMinMemory => "global-min-memory",
            SettingsKey::GlobalMaxMemory => "global-max-memory",
            SettingsKey::GlobalJavaExec => "global-java-exec",
            SettingsKey::EnableGlobalJvmArgs => "enable-global-jvm-args",
            SettingsKey::GlobalJvmArgs => "global-jvm-args",
            SettingsKey::DefaultInstancesPath => "default-instances-path",
            SettingsKey::DefaultLibrariesPath => "default-libraries-path",
            SettingsKey::DefaultAssetsPath => "default-assets-path",
            SettingsKey::ProfileIndexPath => "profile-index-path",
            SettingsKey::InstanceIndexPath => "instance-index-path",
            SettingsKey::ParallelDownloads => "parallel-downloads",
            SettingsKey::DownloadRetries => "download-retries",
            SettingsKey::VerifyDownloads => "verify-downloads",
            SettingsKey::DefaultProfileUuid => "default-profile-uuid",
            SettingsKey::WindowWidth => "window-width",
            SettingsKey::WindowHeight => "window-height",
            SettingsKey::WindowMaximized => "window-maximized",
        }
    }

    pub fn set_string(&self, value: &str) {
        let settings = get_settings();
        settings.set_string(self.name(), value).unwrap();
    }

    pub fn get_string(&self) -> String {
        let settings = get_settings();
        settings.string(self.name()).to_string()
    }

    pub fn set_bool(&self, value: bool) {
        let settings = get_settings();
        settings.set_boolean(self.name(), value).unwrap();
    }

    pub fn get_bool(&self) -> bool {
        let settings = get_settings();
        settings.boolean(self.name())
    }

    pub fn set_integer(&self, value: i32) {
        let settings = get_settings();
        settings.set_int(self.name(), value).unwrap();
    }

    pub fn get_integer(&self) -> i32 {
        let settings = get_settings();
        settings.int(self.name())
    }

    pub fn reset(&self) {
        let settings = get_settings();
        settings.reset(self.name());
    }

    pub fn bind_property<P>(&self, object: &P, property: &str)
    where
        P: IsA<glib::Object>,
    {
        let settings = get_settings();
        settings
            .bind(self.name(), object, property)
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
    }
}

pub fn get_settings() -> gio::Settings {
    gio::Settings::new(config::APP_ID)
}
