use super::GCobbleProfile;
use crate::settings::SettingsKey;
use crate::utils;
use crate::{application::Application, utils::ListStoreExt};
use cobble_core::profile::CobbleProfile;
use gdk::prelude::*;
use gdk::subclass::prelude::*;
use gio::ListStore;
use glib::{clone, ObjectExt, ParamFlags, ParamSpec, ParamSpecObject, StaticType, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use uuid::Uuid;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GProfileManager {
        current_profile: RefCell<Option<GCobbleProfile>>,
        profiles: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GProfileManager {
        const NAME: &'static str = "GProfileManager";
        type Type = super::GProfileManager;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GProfileManager {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::GProfileManager::CURRENT_PROFILE,
                        "Current Profile",
                        "Current Profile",
                        GCobbleProfile::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::GProfileManager::PROFILES,
                        "Profiles",
                        "Profiles",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GProfileManager::CURRENT_PROFILE => {
                    *self.current_profile.borrow_mut() = value.get().unwrap()
                }
                super::GProfileManager::PROFILES => {
                    *self.profiles.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GProfileManager::CURRENT_PROFILE => self.current_profile.borrow().to_value(),
                super::GProfileManager::PROFILES => self.profiles.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GProfileManager(ObjectSubclass<imp::GProfileManager>);
}

impl GProfileManager {
    pub const CURRENT_PROFILE: &str = "current-profile";
    pub const PROFILES: &str = "profiles";

    #[instrument(name = "set_current_profile", skip_all, fields(uuid))]
    pub fn set_current_profile(&self, value: Option<GCobbleProfile>) {
        if let Some(val) = &value {
            let uuid = val.uuid();
            tracing::Span::current().record("uuid", tracing::field::display(uuid));

            trace!("Saving default profile UUID in settings");
            SettingsKey::DefaultProfileUuid.set_string(&uuid.to_string());
        }

        self.set_property(Self::CURRENT_PROFILE, value);
    }

    pub fn current_profile(&self) -> Option<GCobbleProfile> {
        self.property(Self::CURRENT_PROFILE)
    }

    pub fn profiles(&self) -> ListStore {
        self.property(Self::PROFILES)
    }

    pub fn new() -> Self {
        let manager = glib::Object::new::<Self>(&[]);
        manager.init();
        manager
    }

    #[instrument(
        name = "add_profile",
        skip_all,
        fields(
            uuid = %gprofile.uuid(),
        )
    )]
    pub fn add_profile(&self, gprofile: GCobbleProfile) {
        trace!("Appending new profile to list");
        self.profiles().append(&gprofile);
        self.set_current_profile(Some(gprofile.clone()));

        trace!("Saving profile to disk");
        let profile = CobbleProfile::from(gprofile);
        let ctx = glib::MainContext::default();
        ctx.spawn_local(utils::spawn_tokio_future(utils::save_profile(profile)));
    }

    #[instrument(
        name = "add_profile",
        skip_all,
        fields(
            uuid = %uuid,
        )
    )]
    pub fn remove_profile(&self, uuid: Uuid) {
        trace!("Getting index of profile");
        let index = self
            .profiles()
            .index_of::<_, GCobbleProfile>(|x| x.uuid() == uuid);

        match index {
            Some(i) => {
                trace!("Removing profile from list");
                self.profiles().remove(i);

                trace!("Unsetting default profile if profile was default");
                if let Some(current) = self.current_profile() {
                    if current.uuid() == uuid {
                        self.set_current_profile(None);
                    }
                }

                trace!("Removing profile from disk");
                let ctx = glib::MainContext::default();
                ctx.spawn_local(utils::spawn_tokio_future(utils::remove_profile(uuid)));
            }
            None => warn!("Trying to remove unknown profile"),
        }
    }

    #[instrument(name = "init")]
    fn init(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            debug!("Loading profiles from disk");
            let mut profiles = utils::spawn_tokio_future(utils::load_profiles()).await;
            profiles.sort_unstable_by(|a, b| a.player_name.cmp(&b.player_name));
            trace!("Loaded {} profiles", profiles.len());

            trace!("Adding profiles to list");
            let gprofiles = profiles.into_iter().map(GCobbleProfile::from).collect::<Vec<_>>();
            let profiles = this.profiles();
            profiles.splice(0, profiles.n_items(), &gprofiles);

            trace!("Loading default profile from settings");
            let default = SettingsKey::DefaultProfileUuid.get_string();
            let default_profile = gprofiles.into_iter().find(|p| p.uuid().to_string() == default);

            match default_profile {
                Some(profile) => {
                    trace!("Found saved default profile `{}`", profile.uuid());
                    this.set_property(Self::CURRENT_PROFILE, Some(profile));
                },
                None => {
                    trace!("Resetting default profile in settings");
                    SettingsKey::DefaultProfileUuid.reset();
                },
            }
        }));
    }
}

impl Default for GProfileManager {
    fn default() -> Self {
        Application::default().profile_mamanger()
    }
}
