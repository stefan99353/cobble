use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GEnvironmentVariable {
        key: RefCell<String>,
        value: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GEnvironmentVariable {
        const NAME: &'static str = "GEnvironmentVariable";
        type Type = super::GEnvironmentVariable;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GEnvironmentVariable {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GEnvironmentVariable::KEY,
                        "Key",
                        "Key",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GEnvironmentVariable::VALUE,
                        "Value",
                        "Value",
                        None,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GEnvironmentVariable::KEY => *self.key.borrow_mut() = value.get().unwrap(),
                super::GEnvironmentVariable::VALUE => {
                    *self.value.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GEnvironmentVariable::KEY => self.key.borrow().to_value(),
                super::GEnvironmentVariable::VALUE => self.value.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GEnvironmentVariable(ObjectSubclass<imp::GEnvironmentVariable>);
}

impl GEnvironmentVariable {
    pub const KEY: &str = "key";
    pub const VALUE: &str = "value";

    pub fn set_key(&self, value: String) {
        self.set_property(Self::KEY, value);
    }

    pub fn key(&self) -> String {
        self.property(Self::KEY)
    }

    pub fn set_value(&self, value: Option<String>) {
        self.set_property(Self::VALUE, value.unwrap_or_default());
    }

    pub fn value(&self) -> Option<String> {
        let value: String = self.property(Self::VALUE);

        match value.is_empty() {
            true => None,
            false => Some(value),
        }
    }
}

impl From<(String, Option<String>)> for GEnvironmentVariable {
    fn from((key, value): (String, Option<String>)) -> Self {
        glib::Object::new(&[(Self::KEY, &key), (Self::VALUE, &value.unwrap_or_default())])
    }
}

impl From<GEnvironmentVariable> for (String, Option<String>) {
    fn from(value: GEnvironmentVariable) -> Self {
        (value.key(), value.value())
    }
}
