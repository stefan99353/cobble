use cobble_core::minecraft::FabricLoaderInfo;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
mod imp {
    use glib::{ParamSpecBoolean, ParamSpecInt};

    use super::*;

    #[derive(Debug, Default)]
    pub struct GFabricLoaderInfo {
        separator: RefCell<String>,
        build: Cell<i32>,
        maven: RefCell<String>,
        version: RefCell<String>,
        stable: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GFabricLoaderInfo {
        const NAME: &'static str = "GFabricLoaderInfo";
        type Type = super::GFabricLoaderInfo;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GFabricLoaderInfo {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GFabricLoaderInfo::SEPARATOR,
                        "Separator",
                        "Separator",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt::new(
                        super::GFabricLoaderInfo::BUILD,
                        "Build",
                        "Build",
                        i32::MIN,
                        i32::MAX,
                        0,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GFabricLoaderInfo::MAVEN,
                        "Maven",
                        "Maven",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GFabricLoaderInfo::VERSION,
                        "Version",
                        "Version",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GFabricLoaderInfo::STABLE,
                        "Stable",
                        "Stable",
                        false,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GFabricLoaderInfo::SEPARATOR => {
                    *self.separator.borrow_mut() = value.get().unwrap()
                }
                super::GFabricLoaderInfo::BUILD => self.build.set(value.get().unwrap()),
                super::GFabricLoaderInfo::MAVEN => *self.maven.borrow_mut() = value.get().unwrap(),
                super::GFabricLoaderInfo::VERSION => {
                    *self.version.borrow_mut() = value.get().unwrap()
                }
                super::GFabricLoaderInfo::STABLE => self.stable.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GFabricLoaderInfo::SEPARATOR => self.separator.borrow().to_value(),
                super::GFabricLoaderInfo::BUILD => self.build.get().to_value(),
                super::GFabricLoaderInfo::MAVEN => self.maven.borrow().to_value(),
                super::GFabricLoaderInfo::VERSION => self.version.borrow().to_value(),
                super::GFabricLoaderInfo::STABLE => self.stable.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GFabricLoaderInfo(ObjectSubclass<imp::GFabricLoaderInfo>);
}

impl GFabricLoaderInfo {
    pub const SEPARATOR: &str = "separator";
    pub const BUILD: &str = "build";
    pub const MAVEN: &str = "maven";
    pub const VERSION: &str = "version";
    pub const STABLE: &str = "stable";

    pub fn set_separator(&self, value: String) {
        self.set_property(Self::SEPARATOR, value);
    }

    pub fn separator(&self) -> String {
        self.property(Self::SEPARATOR)
    }

    pub fn set_build(&self, value: i32) {
        self.set_property(Self::BUILD, value);
    }

    pub fn build(&self) -> i32 {
        self.property(Self::BUILD)
    }

    pub fn set_maven(&self, value: String) {
        self.set_property(Self::MAVEN, value);
    }

    pub fn maven(&self) -> String {
        self.property(Self::MAVEN)
    }

    pub fn set_version(&self, value: String) {
        self.set_property(Self::VERSION, value);
    }

    pub fn version(&self) -> String {
        self.property(Self::VERSION)
    }

    pub fn set_stable(&self, value: bool) {
        self.set_property(Self::STABLE, value);
    }

    pub fn stable(&self) -> bool {
        self.property(Self::STABLE)
    }
}

impl From<FabricLoaderInfo> for GFabricLoaderInfo {
    fn from(value: FabricLoaderInfo) -> Self {
        glib::Object::new(&[
            (Self::SEPARATOR, &value.separator),
            (Self::BUILD, &value.build),
            (Self::MAVEN, &value.maven),
            (Self::VERSION, &value.version),
            (Self::STABLE, &value.stable),
        ])
    }
}

impl From<GFabricLoaderInfo> for FabricLoaderInfo {
    fn from(value: GFabricLoaderInfo) -> Self {
        Self {
            separator: value.separator(),
            build: value.build(),
            maven: value.maven(),
            version: value.version(),
            stable: value.stable(),
        }
    }
}
