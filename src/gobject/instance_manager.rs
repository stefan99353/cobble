use crate::gobject::GInstance;
use crate::utils;
use crate::{application::Application, utils::ListStoreExt};
use cobble_core::Instance;
use gdk::prelude::*;
use gdk::subclass::prelude::*;
use gio::ListStore;
use glib::{clone, ObjectExt, ParamFlags, ParamSpec, ParamSpecObject, StaticType, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use uuid::Uuid;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GInstanceManager {
        instances: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GInstanceManager {
        const NAME: &'static str = "GInstanceManager";
        type Type = super::GInstanceManager;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GInstanceManager {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::GInstanceManager::INSTANCES,
                    "Instances",
                    "Instances",
                    ListStore::static_type(),
                    ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GInstanceManager::INSTANCES => {
                    *self.instances.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GInstanceManager::INSTANCES => self.instances.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GInstanceManager(ObjectSubclass<imp::GInstanceManager>);
}

impl GInstanceManager {
    pub const INSTANCES: &str = "instances";

    pub fn instances(&self) -> ListStore {
        self.property(Self::INSTANCES)
    }

    pub fn new() -> Self {
        let manager = glib::Object::new::<Self>(&[]);
        manager.init();
        manager
    }

    #[instrument(
        name = "add_instance",
        skip_all,
        fields(
            uuid = %ginstance.uuid(),
            name = ginstance.name(),
            version = ginstance.version(),
        )
    )]
    pub fn add_instance(&self, ginstance: GInstance) {
        trace!("Appending new instance to list");
        self.instances().append(&ginstance);

        trace!("Saving instance to disk");
        let instance = Instance::from(ginstance);
        let ctx = glib::MainContext::default();
        ctx.spawn_local(utils::spawn_tokio_future(utils::save_instance(instance)));
    }

    #[instrument(
        name = "update_instance",
        skip_all,
        fields(
            uuid = %ginstance.uuid(),
            name = ginstance.name(),
            version = ginstance.version(),
        )
    )]
    pub fn update_instance(&self, ginstance: GInstance) {
        let uuid = ginstance.uuid();

        trace!("Getting index of instance");
        let index = self
            .instances()
            .index_of::<_, GInstance>(|x| x.uuid() == uuid);

        match index {
            Some(i) => {
                trace!("Updating instance in list");
                self.instances().splice(i, 1, &[ginstance.clone()]);

                trace!("Saving instance to disk");
                let instance = Instance::from(ginstance);
                let ctx = glib::MainContext::default();
                ctx.spawn_local(utils::spawn_tokio_future(utils::save_instance(instance)));
            }
            None => warn!("Trying to update unknown instance"),
        }
    }

    #[instrument(
        name = "remove_instance",
        skip_all,
        fields(
            uuid = %uuid,
            data = data,
        )
    )]
    pub fn remove_instance(&self, uuid: Uuid, data: bool) {
        trace!("Getting index of instance");
        let index = self
            .instances()
            .index_of::<_, GInstance>(|x| x.uuid() == uuid);

        match index {
            Some(i) => {
                trace!("Removing instance from list");
                self.instances().remove(i);

                trace!("Removing instance from disk");
                let ctx = glib::MainContext::default();
                ctx.spawn_local(utils::spawn_tokio_future(utils::remove_instance(
                    uuid, data,
                )));
            }
            None => warn!("Trying to remove unknown instance"),
        }
    }

    #[instrument(name = "init")]
    fn init(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            debug!("Loading instances from disk");
            let mut instances = utils::spawn_tokio_future(utils::load_instances()).await;
            instances.sort_unstable_by(|a, b| a.created.cmp(&b.created));
            trace!("Loaded {} instances", instances.len());

            trace!("Adding instances to list");
            let ginstances = instances.into_iter().map(GInstance::from).collect::<Vec<_>>();
            this.instances().splice(0, this.instances().n_items(), &ginstances)
        }));
    }
}

impl Default for GInstanceManager {
    fn default() -> Self {
        Application::default().instance_manager()
    }
}
