use crate::gobject::GEnvironmentVariable;
use cobble_core::Instance;
use gdk::prelude::*;
use gdk::subclass::prelude::*;
use gio::ListStore;
use glib::{
    ObjectExt, ParamFlags, ParamSpec, ParamSpecBoolean, ParamSpecInt64, ParamSpecObject,
    ParamSpecString, ParamSpecUInt, StaticType, ToValue,
};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::{Cell, RefCell};
use std::path::PathBuf;
use std::str::FromStr;
use time::OffsetDateTime;
use uuid::Uuid;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GInstance {
        uuid: OnceCell<String>,
        name: RefCell<String>,
        description: RefCell<String>,
        version: RefCell<String>,
        instance_path: RefCell<String>,
        libraries_path: RefCell<String>,
        assets_path: RefCell<String>,
        fullscreen: Cell<bool>,
        enable_custom_window_size: Cell<bool>,
        custom_width: Cell<u32>,
        custom_height: Cell<u32>,
        enable_custom_memory: Cell<bool>,
        custom_min_memory: Cell<u32>,
        custom_max_memory: Cell<u32>,
        custom_java_executable: RefCell<String>,
        custom_jvm_arguments: RefCell<String>,
        environment_variables: RefCell<ListStore>,
        installed: Cell<bool>,
        created: Cell<i64>,
        fabric_version: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GInstance {
        const NAME: &'static str = "GInstance";
        type Type = super::GInstance;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GInstance {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GInstance::UUID,
                        "UUID",
                        "UUID",
                        Some(&Uuid::new_v4().to_string()),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecString::new(
                        super::GInstance::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::DESCRIPTION,
                        "Description",
                        "Description",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::VERSION,
                        "Version",
                        "Version",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::INSTANCE_PATH,
                        "Instance Path",
                        "Instance Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::LIBRARIES_PATH,
                        "Libraries Path",
                        "Libraries Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::ASSETS_PATH,
                        "Assets Path",
                        "Assets Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GInstance::FULLSCREEN,
                        "Fullscreen",
                        "Fullscreen",
                        false,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GInstance::ENABLE_CUSTOM_WINDOW_SIZE,
                        "Enable Custom Window Size",
                        "Enable Custom Window Size",
                        false,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecUInt::new(
                        super::GInstance::CUSTOM_WIDTH,
                        "Custom Width",
                        "Custom Width",
                        u32::MIN,
                        u32::MAX,
                        720,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecUInt::new(
                        super::GInstance::CUSTOM_HEIGHT,
                        "Custom Height",
                        "Custom Height",
                        u32::MIN,
                        u32::MAX,
                        1280,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GInstance::ENABLE_CUSTOM_MEMORY,
                        "Enable Custom Memory",
                        "Enable Custom Memory",
                        false,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecUInt::new(
                        super::GInstance::CUSTOM_MIN_MEMORY,
                        "Custom Min Memory",
                        "Custom Min Memory",
                        u32::MIN,
                        u32::MAX,
                        1024,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecUInt::new(
                        super::GInstance::CUSTOM_MAX_MEMORY,
                        "Custom Max Memory",
                        "Custom Max Memory",
                        u32::MIN,
                        u32::MAX,
                        2048,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::CUSTOM_JAVA_EXECUTABLE,
                        "Custom Java Executable",
                        "Custom Java Executable",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::CUSTOM_JVM_ARGUMENTS,
                        "Custom JVM Arguments",
                        "Custom JVM Arguments",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::GInstance::ENVIRONMENT_VARIABLES,
                        "Environment Variables",
                        "Environment Variables",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GInstance::INSTALLED,
                        "Installed",
                        "Installed",
                        false,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GInstance::CREATED,
                        "Created",
                        "Created",
                        i64::MIN,
                        i64::MAX,
                        OffsetDateTime::now_utc().unix_timestamp(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GInstance::FABRIC_VERSION,
                        "Fabric Version",
                        "Fabric Version",
                        None,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            match pspec.name() {
                super::GInstance::UUID => self.uuid.set(value.get().unwrap()).unwrap(),
                super::GInstance::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GInstance::DESCRIPTION => {
                    *self.description.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::VERSION => *self.version.borrow_mut() = value.get().unwrap(),
                super::GInstance::INSTANCE_PATH => {
                    *self.instance_path.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::LIBRARIES_PATH => {
                    *self.libraries_path.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::ASSETS_PATH => {
                    *self.assets_path.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::FULLSCREEN => self.fullscreen.set(value.get().unwrap()),
                super::GInstance::ENABLE_CUSTOM_WINDOW_SIZE => {
                    self.enable_custom_window_size.set(value.get().unwrap())
                }
                super::GInstance::CUSTOM_WIDTH => self.custom_width.set(value.get().unwrap()),
                super::GInstance::CUSTOM_HEIGHT => self.custom_height.set(value.get().unwrap()),
                super::GInstance::ENABLE_CUSTOM_MEMORY => {
                    self.enable_custom_memory.set(value.get().unwrap())
                }
                super::GInstance::CUSTOM_MIN_MEMORY => {
                    self.custom_min_memory.set(value.get().unwrap())
                }
                super::GInstance::CUSTOM_MAX_MEMORY => {
                    self.custom_max_memory.set(value.get().unwrap())
                }
                super::GInstance::CUSTOM_JAVA_EXECUTABLE => {
                    *self.custom_java_executable.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::CUSTOM_JVM_ARGUMENTS => {
                    *self.custom_jvm_arguments.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::ENVIRONMENT_VARIABLES => {
                    *self.environment_variables.borrow_mut() = value.get().unwrap()
                }
                super::GInstance::INSTALLED => self.installed.set(value.get().unwrap()),
                super::GInstance::CREATED => self.created.set(value.get().unwrap()),
                super::GInstance::FABRIC_VERSION => {
                    *self.fabric_version.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GInstance::UUID => self.uuid.get().to_value(),
                super::GInstance::NAME => self.name.borrow().to_value(),
                super::GInstance::DESCRIPTION => self.description.borrow().to_value(),
                super::GInstance::VERSION => self.version.borrow().to_value(),
                super::GInstance::INSTANCE_PATH => self.instance_path.borrow().to_value(),
                super::GInstance::LIBRARIES_PATH => self.libraries_path.borrow().to_value(),
                super::GInstance::ASSETS_PATH => self.assets_path.borrow().to_value(),
                super::GInstance::FULLSCREEN => self.fullscreen.get().to_value(),
                super::GInstance::ENABLE_CUSTOM_WINDOW_SIZE => {
                    self.enable_custom_window_size.get().to_value()
                }
                super::GInstance::CUSTOM_WIDTH => self.custom_width.get().to_value(),
                super::GInstance::CUSTOM_HEIGHT => self.custom_height.get().to_value(),
                super::GInstance::ENABLE_CUSTOM_MEMORY => {
                    self.enable_custom_memory.get().to_value()
                }
                super::GInstance::CUSTOM_MIN_MEMORY => self.custom_min_memory.get().to_value(),
                super::GInstance::CUSTOM_MAX_MEMORY => self.custom_max_memory.get().to_value(),
                super::GInstance::CUSTOM_JAVA_EXECUTABLE => {
                    self.custom_java_executable.borrow().to_value()
                }
                super::GInstance::CUSTOM_JVM_ARGUMENTS => {
                    self.custom_jvm_arguments.borrow().to_value()
                }
                super::GInstance::ENVIRONMENT_VARIABLES => {
                    self.environment_variables.borrow().to_value()
                }
                super::GInstance::INSTALLED => self.installed.get().to_value(),
                super::GInstance::CREATED => self.created.get().to_value(),
                super::GInstance::FABRIC_VERSION => self.fabric_version.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GInstance(ObjectSubclass<imp::GInstance>);
}

impl GInstance {
    pub const UUID: &str = "uuid";
    pub const NAME: &str = "name";
    pub const DESCRIPTION: &str = "description";
    pub const VERSION: &str = "version";
    pub const INSTANCE_PATH: &str = "instance-path";
    pub const LIBRARIES_PATH: &str = "libraries-path";
    pub const ASSETS_PATH: &str = "assets-path";
    pub const FULLSCREEN: &str = "fullscreen";
    pub const ENABLE_CUSTOM_WINDOW_SIZE: &str = "enable-custom-window-size";
    pub const CUSTOM_WIDTH: &str = "custom-width";
    pub const CUSTOM_HEIGHT: &str = "custom-height";
    pub const ENABLE_CUSTOM_MEMORY: &str = "enable-custom-memory";
    pub const CUSTOM_MIN_MEMORY: &str = "custom-min-memory";
    pub const CUSTOM_MAX_MEMORY: &str = "custom-max-memory";
    pub const CUSTOM_JAVA_EXECUTABLE: &str = "custom-java-executable";
    pub const CUSTOM_JVM_ARGUMENTS: &str = "custom-jvm-arguments";
    pub const ENVIRONMENT_VARIABLES: &str = "environment-variables";
    pub const INSTALLED: &str = "installed";
    pub const CREATED: &str = "created";
    pub const FABRIC_VERSION: &str = "fabric-version";

    pub fn uuid(&self) -> Uuid {
        let uuid: String = self.property(Self::UUID);
        Uuid::from_str(&uuid).unwrap()
    }

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_description(&self, value: Option<String>) {
        self.set_property(Self::DESCRIPTION, value.unwrap_or_default());
    }

    pub fn description(&self) -> Option<String> {
        let value: String = self.property(Self::DESCRIPTION);

        match value.is_empty() {
            true => None,
            false => Some(value),
        }
    }

    pub fn set_version(&self, value: String) {
        self.set_property(Self::VERSION, value);
    }

    pub fn version(&self) -> String {
        self.property(Self::VERSION)
    }

    pub fn set_instance_path(&self, value: PathBuf) {
        self.set_property(Self::INSTANCE_PATH, value.to_string_lossy().to_string());
    }

    pub fn instance_path(&self) -> PathBuf {
        let path: String = self.property(Self::INSTANCE_PATH);
        PathBuf::from(path)
    }

    pub fn set_libraries_path(&self, value: PathBuf) {
        self.set_property(Self::LIBRARIES_PATH, value.to_string_lossy().to_string());
    }

    pub fn libraries_path(&self) -> PathBuf {
        let path: String = self.property(Self::LIBRARIES_PATH);
        PathBuf::from(path)
    }

    pub fn set_assets_path(&self, value: PathBuf) {
        self.set_property(Self::ASSETS_PATH, value.to_string_lossy().to_string());
    }

    pub fn assets_path(&self) -> PathBuf {
        let path: String = self.property(Self::ASSETS_PATH);
        PathBuf::from(path)
    }

    pub fn set_fullscreen(&self, value: bool) {
        self.set_property(Self::FULLSCREEN, value);
    }

    pub fn fullscreen(&self) -> bool {
        self.property(Self::FULLSCREEN)
    }

    pub fn set_enable_custom_window_size(&self, value: bool) {
        self.set_property(Self::ENABLE_CUSTOM_WINDOW_SIZE, value);
    }

    pub fn enable_custom_window_size(&self) -> bool {
        self.property(Self::ENABLE_CUSTOM_WINDOW_SIZE)
    }

    pub fn set_custom_width(&self, value: u32) {
        self.set_property(Self::CUSTOM_WIDTH, value);
    }

    pub fn custom_width(&self) -> u32 {
        self.property(Self::CUSTOM_WIDTH)
    }

    pub fn set_custom_height(&self, value: u32) {
        self.set_property(Self::CUSTOM_HEIGHT, value);
    }

    pub fn custom_height(&self) -> u32 {
        self.property(Self::CUSTOM_HEIGHT)
    }

    pub fn set_enable_custom_memory(&self, value: bool) {
        self.set_property(Self::ENABLE_CUSTOM_MEMORY, value);
    }

    pub fn enable_custom_memory(&self) -> bool {
        self.property(Self::ENABLE_CUSTOM_MEMORY)
    }

    pub fn set_custom_min_memory(&self, value: u32) {
        self.set_property(Self::CUSTOM_MIN_MEMORY, value);
    }

    pub fn custom_min_memory(&self) -> u32 {
        self.property(Self::CUSTOM_MIN_MEMORY)
    }

    pub fn set_custom_max_memory(&self, value: u32) {
        self.set_property(Self::CUSTOM_MAX_MEMORY, value);
    }

    pub fn custom_max_memory(&self) -> u32 {
        self.property(Self::CUSTOM_MAX_MEMORY)
    }

    pub fn set_custom_java_executable(&self, value: Option<String>) {
        self.set_property(Self::CUSTOM_JAVA_EXECUTABLE, value.unwrap_or_default());
    }

    pub fn custom_java_executable(&self) -> Option<String> {
        let value: String = self.property(Self::CUSTOM_JAVA_EXECUTABLE);

        match value.is_empty() {
            true => None,
            false => Some(value),
        }
    }

    pub fn set_custom_jvm_arguments(&self, value: Option<String>) {
        self.set_property(Self::CUSTOM_JVM_ARGUMENTS, value.unwrap_or_default());
    }

    pub fn custom_jvm_arguments(&self) -> Option<String> {
        let value: String = self.property(Self::CUSTOM_JVM_ARGUMENTS);

        match value.is_empty() {
            true => None,
            false => Some(value),
        }
    }

    pub fn environment_variables(&self) -> ListStore {
        self.property(Self::ENVIRONMENT_VARIABLES)
    }

    pub fn set_environment_variables(&self, value: Vec<(String, Option<String>)>) {
        let vars = self.environment_variables();
        let new_vars = value
            .into_iter()
            .map(GEnvironmentVariable::from)
            .collect::<Vec<_>>();
        vars.splice(0, vars.n_items(), &new_vars)
    }

    pub fn environment_variables_vec(&self) -> Vec<(String, Option<String>)> {
        let mut result = vec![];

        let vars = self.environment_variables();
        for pos in 0..vars.n_items() {
            let var = vars
                .item(pos)
                .unwrap()
                .downcast::<GEnvironmentVariable>()
                .unwrap();

            result.push(var.into());
        }

        result
    }

    pub fn set_installed(&self, value: bool) {
        self.set_property(Self::INSTALLED, value);
    }

    pub fn installed(&self) -> bool {
        self.property(Self::INSTALLED)
    }

    pub fn set_created(&self, value: OffsetDateTime) {
        self.set_property(Self::CREATED, value.unix_timestamp());
    }

    pub fn created(&self) -> OffsetDateTime {
        let timestamp: i64 = self.property(Self::CREATED);
        OffsetDateTime::from_unix_timestamp(timestamp).unwrap()
    }

    pub fn set_fabric_version(&self, value: Option<String>) {
        self.set_property(Self::FABRIC_VERSION, value.unwrap_or_default());
    }

    pub fn fabric_version(&self) -> Option<String> {
        let value: String = self.property(Self::FABRIC_VERSION);

        match value.is_empty() {
            true => None,
            false => Some(value),
        }
    }
}

impl Default for GInstance {
    fn default() -> Self {
        glib::Object::new(&[
            (Self::UUID, &Uuid::new_v4().to_string().to_uppercase()),
            (Self::CUSTOM_WIDTH, &1280_u32),
            (Self::CUSTOM_HEIGHT, &720_u32),
            (Self::CUSTOM_MIN_MEMORY, &1024_u32),
            (Self::CUSTOM_MAX_MEMORY, &2048_u32),
            (Self::CREATED, &OffsetDateTime::now_utc().unix_timestamp()),
        ])
    }
}

impl From<Instance> for GInstance {
    fn from(value: Instance) -> Self {
        let object: Self = glib::Object::new(&[
            (Self::UUID, &value.uuid.to_string().to_uppercase()),
            (Self::NAME, &value.name),
            (Self::DESCRIPTION, &value.description.unwrap_or_default()),
            (Self::VERSION, &value.version),
            (
                Self::INSTANCE_PATH,
                &value.instance_path.to_string_lossy().to_string(),
            ),
            (
                Self::LIBRARIES_PATH,
                &value.libraries_path.to_string_lossy().to_string(),
            ),
            (
                Self::ASSETS_PATH,
                &value.assets_path.to_string_lossy().to_string(),
            ),
            (Self::FULLSCREEN, &value.fullscreen),
            (
                Self::ENABLE_CUSTOM_WINDOW_SIZE,
                &value.enable_custom_window_size,
            ),
            (Self::CUSTOM_WIDTH, &value.custom_width),
            (Self::CUSTOM_HEIGHT, &value.custom_height),
            (Self::ENABLE_CUSTOM_MEMORY, &value.enable_custom_memory),
            (Self::CUSTOM_MIN_MEMORY, &value.custom_min_memory),
            (Self::CUSTOM_MAX_MEMORY, &value.custom_max_memory),
            (
                Self::CUSTOM_JAVA_EXECUTABLE,
                &value.custom_java_executable.unwrap_or_default(),
            ),
            (
                Self::CUSTOM_JVM_ARGUMENTS,
                &value.custom_jvm_arguments.unwrap_or_default(),
            ),
            (Self::INSTALLED, &value.installed),
            (Self::CREATED, &value.created.unix_timestamp()),
            (
                Self::FABRIC_VERSION,
                &value.fabric_version.unwrap_or_default(),
            ),
        ]);

        object.set_environment_variables(value.environment_variables);

        object
    }
}

impl From<GInstance> for Instance {
    fn from(value: GInstance) -> Self {
        Self {
            uuid: value.uuid(),
            name: value.name(),
            description: value.description(),
            version: value.version(),
            instance_path: value.instance_path(),
            libraries_path: value.libraries_path(),
            assets_path: value.assets_path(),
            fullscreen: value.fullscreen(),
            enable_custom_window_size: value.enable_custom_window_size(),
            custom_width: value.custom_width(),
            custom_height: value.custom_height(),
            enable_custom_memory: value.enable_custom_memory(),
            custom_min_memory: value.custom_min_memory(),
            custom_max_memory: value.custom_max_memory(),
            custom_java_executable: value.custom_java_executable(),
            custom_jvm_arguments: value.custom_jvm_arguments(),
            environment_variables: value.environment_variables_vec(),
            installed: value.installed(),
            created: value.created(),
            fabric_version: value.fabric_version(),
        }
    }
}
