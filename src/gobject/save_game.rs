use cobble_core::minecraft::Difficulty;
use cobble_core::minecraft::GameType;
use cobble_core::minecraft::SaveGame;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecInt64, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::Cell;
use std::cell::RefCell;
use std::path::PathBuf;
use time::OffsetDateTime;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GSaveGame {
        name: RefCell<String>,
        path: RefCell<String>,
        difficulty: RefCell<Option<String>>,
        game_type: RefCell<String>,
        game_version: RefCell<Option<String>>,
        seed: RefCell<Option<String>>,
        last_played: Cell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GSaveGame {
        const NAME: &'static str = "GSaveGame";
        type Type = super::GSaveGame;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GSaveGame {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GSaveGame::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GSaveGame::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GSaveGame::DIFFICULTY,
                        "Difficulty",
                        "Difficulty",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GSaveGame::GAME_TYPE,
                        "Game Type",
                        "Game Type",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GSaveGame::GAME_VERSION,
                        "Game Version",
                        "Game Version",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GSaveGame::SEED,
                        "Seed",
                        "Seed",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GSaveGame::LAST_PLAYED,
                        "Last Played",
                        "Last Played",
                        i64::MIN,
                        i64::MAX,
                        OffsetDateTime::now_utc().unix_timestamp(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GSaveGame::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GSaveGame::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                super::GSaveGame::DIFFICULTY => {
                    *self.difficulty.borrow_mut() = value.get().unwrap()
                }
                super::GSaveGame::GAME_TYPE => *self.game_type.borrow_mut() = value.get().unwrap(),
                super::GSaveGame::GAME_VERSION => {
                    *self.game_version.borrow_mut() = value.get().unwrap()
                }
                super::GSaveGame::SEED => *self.seed.borrow_mut() = value.get().unwrap(),
                super::GSaveGame::LAST_PLAYED => self.last_played.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GSaveGame::NAME => self.name.borrow().to_value(),
                super::GSaveGame::PATH => self.path.borrow().to_value(),
                super::GSaveGame::DIFFICULTY => self.difficulty.borrow().to_value(),
                super::GSaveGame::GAME_TYPE => self.game_type.borrow().to_value(),
                super::GSaveGame::GAME_VERSION => self.game_version.borrow().to_value(),
                super::GSaveGame::SEED => self.seed.borrow().to_value(),
                super::GSaveGame::LAST_PLAYED => self.last_played.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GSaveGame(ObjectSubclass<imp::GSaveGame>);
}

impl GSaveGame {
    pub const NAME: &str = "name";
    pub const PATH: &str = "path";
    pub const DIFFICULTY: &str = "difficulty";
    pub const GAME_TYPE: &str = "game-type";
    pub const GAME_VERSION: &str = "game-version";
    pub const SEED: &str = "seed";
    pub const LAST_PLAYED: &str = "last-played";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }

    pub fn set_difficulty(&self, value: Option<Difficulty>) {
        let value = value.map(|d| match d {
            Difficulty::Peaceful => "peaceful",
            Difficulty::Easy => "easy",
            Difficulty::Normal => "normal",
            Difficulty::Hard => "hard",
        });
        self.set_property(Self::DIFFICULTY, value);
    }

    pub fn difficulty(&self) -> Option<Difficulty> {
        let value: Option<String> = self.property(Self::DIFFICULTY);

        value.and_then(|d| match d.as_str() {
            "peaceful" => Some(Difficulty::Peaceful),
            "easy" => Some(Difficulty::Easy),
            "normal" => Some(Difficulty::Normal),
            "hard" => Some(Difficulty::Hard),
            _ => None,
        })
    }

    pub fn set_game_type(&self, value: GameType) {
        let value = match value {
            GameType::Survival => "survival",
            GameType::Creative => "creative",
            GameType::Adventure => "adventure",
            GameType::Spectator => "spectator",
        };
        self.set_property(Self::GAME_TYPE, value);
    }

    pub fn game_type(&self) -> GameType {
        let value: String = self.property(Self::GAME_TYPE);

        match value.as_str() {
            "survival" => GameType::Survival,
            "creative" => GameType::Creative,
            "adventure" => GameType::Adventure,
            "spectator" => GameType::Spectator,
            g => panic!("Invalid game type string '{g}'"),
        }
    }

    pub fn set_game_version(&self, value: Option<String>) {
        self.set_property(Self::GAME_VERSION, value);
    }

    pub fn game_version(&self) -> Option<String> {
        self.property(Self::GAME_VERSION)
    }

    pub fn set_seed(&self, value: Option<i64>) {
        let value = value.map(|s| s.to_string());
        self.set_property(Self::SEED, value);
    }

    pub fn seed(&self) -> Option<i64> {
        let value: Option<String> = self.property(Self::SEED);
        value.map(|s| s.parse::<i64>().unwrap())
    }

    pub fn set_last_played(&self, value: OffsetDateTime) {
        self.set_property(Self::LAST_PLAYED, value.unix_timestamp());
    }

    pub fn last_played(&self) -> OffsetDateTime {
        let value: i64 = self.property(Self::LAST_PLAYED);
        OffsetDateTime::from_unix_timestamp(value).unwrap()
    }
}

impl From<SaveGame> for GSaveGame {
    fn from(value: SaveGame) -> Self {
        let object: Self = glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
            (Self::GAME_VERSION, &value.game_version),
            (Self::SEED, &value.seed.map(|s| s.to_string())),
            (Self::LAST_PLAYED, &value.last_played.unix_timestamp()),
        ]);

        object.set_difficulty(value.difficulty);
        object.set_game_type(value.game_type);

        object
    }
}

impl From<GSaveGame> for SaveGame {
    fn from(value: GSaveGame) -> Self {
        Self {
            name: value.name(),
            path: value.path(),
            difficulty: value.difficulty(),
            game_type: value.game_type(),
            game_version: value.game_version(),
            seed: value.seed(),
            last_played: value.last_played(),
        }
    }
}
