use cobble_core::minecraft::models::VersionSummary;
use cobble_core::minecraft::models::VersionType;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecInt64, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::Cell;
use std::cell::RefCell;
use time::OffsetDateTime;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GVersionSummary {
        id: RefCell<String>,
        type_: RefCell<String>,
        url: RefCell<String>,
        time: Cell<i64>,
        release_time: Cell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GVersionSummary {
        const NAME: &'static str = "GVersionSummary";
        type Type = super::GVersionSummary;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GVersionSummary {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GVersionSummary::ID,
                        "Id",
                        "Id",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GVersionSummary::TYPE,
                        "Type",
                        "Type",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GVersionSummary::URL,
                        "Url",
                        "Url",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GVersionSummary::TIME,
                        "Time",
                        "Time",
                        i64::MIN,
                        i64::MAX,
                        OffsetDateTime::now_utc().unix_timestamp(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GVersionSummary::RELEASE_TIME,
                        "Time",
                        "Time",
                        i64::MIN,
                        i64::MAX,
                        OffsetDateTime::now_utc().unix_timestamp(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GVersionSummary::ID => *self.id.borrow_mut() = value.get().unwrap(),
                super::GVersionSummary::TYPE => *self.type_.borrow_mut() = value.get().unwrap(),
                super::GVersionSummary::URL => *self.url.borrow_mut() = value.get().unwrap(),
                super::GVersionSummary::TIME => self.time.set(value.get().unwrap()),
                super::GVersionSummary::RELEASE_TIME => self.release_time.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GVersionSummary::ID => self.id.borrow().to_value(),
                super::GVersionSummary::TYPE => self.type_.borrow().to_value(),
                super::GVersionSummary::URL => self.url.borrow().to_value(),
                super::GVersionSummary::TIME => self.time.get().to_value(),
                super::GVersionSummary::RELEASE_TIME => self.release_time.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GVersionSummary(ObjectSubclass<imp::GVersionSummary>);
}

impl GVersionSummary {
    pub const ID: &str = "id";
    pub const TYPE: &str = "type";
    pub const URL: &str = "url";
    pub const TIME: &str = "time";
    pub const RELEASE_TIME: &str = "release-time";

    pub fn set_id(&self, value: String) {
        self.set_property(Self::ID, value);
    }

    pub fn id(&self) -> String {
        self.property(Self::ID)
    }

    pub fn set_type(&self, value: VersionType) {
        let value = match value {
            VersionType::Release => "release",
            VersionType::Snapshot => "snapshot",
            VersionType::OldAlpha => "old_alpha",
            VersionType::OldBeta => "old_beta",
        };

        self.set_property(Self::TYPE, value);
    }

    pub fn type_(&self) -> VersionType {
        let value: String = self.property(Self::TYPE);
        match value.as_str() {
            "release" => VersionType::Release,
            "snapshot" => VersionType::Snapshot,
            "old_alpha" => VersionType::OldAlpha,
            "old_beta" => VersionType::OldBeta,
            _ => panic!("Invalid version type"),
        }
    }

    pub fn set_url(&self, value: String) {
        self.set_property(Self::URL, value);
    }

    pub fn url(&self) -> String {
        self.property(Self::URL)
    }

    pub fn set_time(&self, value: OffsetDateTime) {
        self.set_property(Self::TIME, value.unix_timestamp());
    }

    pub fn time(&self) -> OffsetDateTime {
        let timestamp: i64 = self.property(Self::TIME);
        OffsetDateTime::from_unix_timestamp(timestamp).unwrap()
    }

    pub fn set_release_time(&self, value: OffsetDateTime) {
        self.set_property(Self::RELEASE_TIME, value.unix_timestamp());
    }

    pub fn release_time(&self) -> OffsetDateTime {
        let timestamp: i64 = self.property(Self::RELEASE_TIME);
        OffsetDateTime::from_unix_timestamp(timestamp).unwrap()
    }
}

impl From<VersionSummary> for GVersionSummary {
    fn from(value: VersionSummary) -> Self {
        let type_ = match value._type {
            VersionType::Release => "release",
            VersionType::Snapshot => "snapshot",
            VersionType::OldAlpha => "old_alpha",
            VersionType::OldBeta => "old_beta",
        };

        glib::Object::new(&[
            (Self::ID, &value.id),
            (Self::TYPE, &type_),
            (Self::URL, &value.url),
            (Self::TIME, &value.time.unix_timestamp()),
            (Self::RELEASE_TIME, &value.release_time.unix_timestamp()),
        ])
    }
}

impl From<GVersionSummary> for VersionSummary {
    fn from(value: GVersionSummary) -> Self {
        Self {
            id: value.id(),
            _type: value.type_(),
            url: value.url(),
            time: value.time(),
            release_time: value.release_time(),
        }
    }
}
