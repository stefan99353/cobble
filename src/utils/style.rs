use glib::IsA;
use gtk::traits::WidgetExt;

pub fn conditional_style<T: IsA<gtk::Widget>>(condition: bool, widget: &T, klass: &str) {
    if condition {
        widget.add_css_class(klass);
    } else {
        widget.remove_css_class(klass);
    }
}
