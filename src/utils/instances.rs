use crate::settings::SettingsKey;
use crate::utils::{error_dialog, read_file, write_file};
use cobble_core::Instance;
use std::{collections::HashMap, path::PathBuf};
use uuid::Uuid;

type InstanceStorage = HashMap<Uuid, Instance>;

#[instrument(
    name = "save_instance",
    skip_all,
    fields(
        uuid = %instance.uuid,
        instance_index,
    )
)]
pub async fn save_instance(instance: Instance) {
    trace!("Getting instance index path");
    let path = PathBuf::from(SettingsKey::InstanceIndexPath.get_string());
    tracing::Span::current().record("instance_index", tracing::field::display(path.display()));

    trace!("Deserializing instances from index");
    let mut saved_instances: InstanceStorage = match read_file(&path).await {
        Ok(instances) => instances,
        Err(err) => {
            error_dialog(err);
            return;
        }
    };

    trace!("Inserting new instance (possibly replacing old instance)");
    let _old_instance = saved_instances.insert(instance.uuid, instance);

    trace!("Serializing instances into index");
    if let Err(err) = write_file(&path, &saved_instances).await {
        error_dialog(err);
    }
}

#[instrument(name = "load_instances", skip_all, fields(instance_index,))]
pub async fn load_instances() -> Vec<Instance> {
    trace!("Getting instance index path");
    let path = PathBuf::from(SettingsKey::InstanceIndexPath.get_string());
    tracing::Span::current().record("instance_index", tracing::field::display(path.display()));

    trace!("Deserializing instances from index");
    let saved_instances: InstanceStorage = match read_file(&path).await {
        Ok(instances) => instances,
        Err(err) => {
            error_dialog(err);
            return vec![];
        }
    };

    saved_instances.into_values().collect()
}

#[instrument(
    name = "remove_instance",
    skip_all,
    fields(
        uuid = %uuid,
        instance_index,
    )
)]
pub async fn remove_instance(uuid: Uuid, with_data: bool) {
    trace!("Getting instance index path");
    let path = PathBuf::from(SettingsKey::InstanceIndexPath.get_string());
    tracing::Span::current().record("instance_index", tracing::field::display(path.display()));

    trace!("Deserializing instances from index");
    let mut saved_instances: InstanceStorage = match read_file(&path).await {
        Ok(instances) => instances,
        Err(err) => {
            error_dialog(err);
            return;
        }
    };

    trace!("Removing instance");
    let old_instance = saved_instances.remove(&uuid);

    trace!("Serializing instances into index");
    if let Err(err) = write_file(&path, &saved_instances).await {
        error_dialog(err);
    }

    if let (Some(instance), true) = (old_instance, with_data) {
        trace!("Removing instance data");
        if let Err(err) = instance.remove().await {
            error_dialog(err);
        }
    }
}
