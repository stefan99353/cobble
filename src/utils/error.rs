use std::fmt::Display;

pub fn error_dialog<E>(err: E)
where
    E: Display,
{
    error!("{}", err);
}
