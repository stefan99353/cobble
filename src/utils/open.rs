use std::path::PathBuf;

use super::{spawn_tokio, spawn_tokio_future};

pub fn open_path(path: PathBuf) {
    let ctx = glib::MainContext::default();
    ctx.spawn_local(async move {
        if !path.exists() {
            let create_path = path.clone();
            let fut = tokio::fs::create_dir_all(create_path);

            if let Err(err) = spawn_tokio_future(fut).await {
                error!("{err}");
            }
        }

        if let Err(err) = spawn_tokio(move || open::that(path)).await {
            error!("{err}");
        }
    });
}
