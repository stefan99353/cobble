use crate::settings::SettingsKey;
use crate::utils::{error_dialog, read_file, write_file};
use cobble_core::profile::CobbleProfile;
use std::{collections::HashMap, path::PathBuf};
use time::OffsetDateTime;
use uuid::Uuid;

type ProfileStorage = HashMap<Uuid, CobbleProfile>;

#[instrument(
    name = "save_profile",
    skip_all,
    fields(
        uuid = %profile.uuid,
        profile_index,
    )
)]
pub async fn save_profile(profile: CobbleProfile) {
    trace!("Getting profile index path");
    let path = PathBuf::from(SettingsKey::ProfileIndexPath.get_string());
    tracing::Span::current().record("profile_index", tracing::field::display(path.display()));

    trace!("Deserializing profiles from index");
    let mut saved_profiles: ProfileStorage = match read_file(&path).await {
        Ok(profiles) => profiles,
        Err(err) => {
            error_dialog(err);
            return;
        }
    };

    trace!("Inserting new profile (possibly replacing old profile)");
    let _old_profile = saved_profiles.insert(profile.uuid, profile);

    trace!("Serializing profiles into index");
    if let Err(err) = write_file(&path, &saved_profiles).await {
        error_dialog(err);
    }
}

#[instrument(name = "load_profiles", skip_all, fields(profile_index,))]
pub async fn load_profiles() -> Vec<CobbleProfile> {
    trace!("Getting profile index path");
    let path = PathBuf::from(SettingsKey::ProfileIndexPath.get_string());
    tracing::Span::current().record("profile_index", tracing::field::display(path.display()));

    trace!("Deserializing profiles from index");
    let saved_profiles: ProfileStorage = match read_file(&path).await {
        Ok(profiles) => profiles,
        Err(err) => {
            error_dialog(err);
            return vec![];
        }
    };

    saved_profiles.into_values().collect()
}

#[instrument(
    name = "remove_profile",
    skip_all,
    fields(
        uuid = %uuid,
        profile_index,
    )
)]
pub async fn remove_profile(uuid: Uuid) {
    trace!("Getting profile index path");
    let path = PathBuf::from(SettingsKey::ProfileIndexPath.get_string());
    tracing::Span::current().record("profile_index", tracing::field::display(path.display()));

    trace!("Deserializing profiles from index");
    let mut saved_profiles: ProfileStorage = match read_file(&path).await {
        Ok(profiles) => profiles,
        Err(err) => {
            error_dialog(err);
            return;
        }
    };

    trace!("Removing profile");
    let _old_profile = saved_profiles.remove(&uuid);

    trace!("Serializing profiles into index");
    if let Err(err) = write_file(&path, &saved_profiles).await {
        error_dialog(err);
    }
}

#[instrument(
    name = "refresh_and_save_profile",
    skip_all,
    fields(
        uuid = %profile.uuid,
    )
)]
pub async fn refresh_and_save_profile(profile: &mut CobbleProfile) {
    trace!("Check if token expired");
    if profile.minecraft_token_exp <= OffsetDateTime::now_utc() {
        trace!("Refreshing profile token");
        let result = profile
            .refresh(crate::config::MS_GRAPH_ID.to_string())
            .await;

        if let Err(err) = result {
            error_dialog(err);
        }

        trace!("Saving profile");
        save_profile(profile.clone()).await;
    }
}
