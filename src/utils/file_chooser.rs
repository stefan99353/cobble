use glib::{clone, IsA};
use gtk::{prelude::*, FileFilter};
use gtk::{FileChooserAction, FileChooserNative, ResponseType};
use std::path::PathBuf;

pub fn native_file_chooser(
    title: Option<&str>,
    parent: Option<&impl IsA<gtk::Window>>,
    action: FileChooserAction,
    accept_label: Option<&str>,
    cancel_label: Option<&str>,
    filters: &[FileFilter],
    modal: bool,
) -> glib::Receiver<Option<PathBuf>> {
    let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    let dialog = FileChooserNative::new(title, parent, action, accept_label, cancel_label);
    dialog.set_modal(modal);

    for filter in filters {
        dialog.add_filter(filter);
    }

    dialog.connect_response(
        clone!(@strong dialog, @strong sender => move |_, response| {
            dialog.destroy();
            let mut result = None;

            if response == ResponseType::Accept {
                if let Some(file) = dialog.file() {
                    result = file.path();
                }
            }

            if let Err(err) = sender.send(result) {
                error!("Failed to send path of file/folder: {}", err);
            }
        }),
    );

    dialog.show();
    receiver
}

pub async fn native_file_chooser_async(
    title: Option<&str>,
    parent: Option<&impl IsA<gtk::Window>>,
    action: FileChooserAction,
    accept_label: Option<&str>,
    cancel_label: Option<&str>,
    filters: &[FileFilter],
    modal: bool,
) -> Option<PathBuf> {
    // Build Dialog
    let dialog = FileChooserNative::new(title, parent, action, accept_label, cancel_label);
    dialog.set_modal(modal);
    for filter in filters {
        dialog.add_filter(filter);
    }

    // Response
    let (sender, mut receiver) = tokio::sync::mpsc::channel(1);
    dialog.connect_response(move |dialog, response| {
        dialog.destroy();

        let mut result = None;
        if response == ResponseType::Accept {
            if let Some(file) = dialog.file() {
                result = file.path();
            }
        }

        let ctx = glib::MainContext::default();
        let sender = sender.clone();
        ctx.spawn_local(async move {
            sender.send(result).await.unwrap();
        });
    });

    dialog.show();

    receiver.recv().await.unwrap()
}
