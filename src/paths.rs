use crate::settings::SettingsKey;
use lazy_static::lazy_static;
use std::fs;
use std::path::PathBuf;

lazy_static! {
    static ref DATA: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path
    };
    pub static ref CACHE: PathBuf = {
        let mut path = glib::user_cache_dir();
        path.push("cobble");
        path
    };
    static ref DEFAULT_INSTANCES_PATH: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path.push("instances");
        path
    };
    static ref DEFAULT_LIBRARIES_PATH: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path.push("libraries");
        path
    };
    static ref DEFAULT_ASSETS_PATH: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path.push("assets");
        path
    };
    static ref PROFILES_FILE_PATH: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path.push("profiles.json");
        path
    };
    static ref INSTANCES_FILE_PATH: PathBuf = {
        let mut path = glib::user_data_dir();
        path.push("cobble");
        path.push("instances.json");
        path
    };
}

#[instrument(
    name = "init_paths",
    level = "trace",
    skip_all,
    fields(
        data = %DATA.to_string_lossy(),
        cache = %CACHE.to_string_lossy(),
    )
)]
pub fn init() -> Result<(), std::io::Error> {
    trace!("Creating data dir");
    fs::create_dir_all(DATA.as_path())?;
    trace!("Creating cache dir");
    fs::create_dir_all(CACHE.as_path())?;
    trace!("Setting default paths");
    set_defaults();

    Ok(())
}

fn set_defaults() {
    let default_instances_path = SettingsKey::DefaultInstancesPath.get_string();
    if default_instances_path.is_empty() {
        SettingsKey::DefaultInstancesPath.set_string(&DEFAULT_INSTANCES_PATH.to_string_lossy())
    }

    let default_libraries_path = SettingsKey::DefaultLibrariesPath.get_string();
    if default_libraries_path.is_empty() {
        SettingsKey::DefaultLibrariesPath.set_string(&DEFAULT_LIBRARIES_PATH.to_string_lossy())
    }

    let default_assets_path = SettingsKey::DefaultAssetsPath.get_string();
    if default_assets_path.is_empty() {
        SettingsKey::DefaultAssetsPath.set_string(&DEFAULT_ASSETS_PATH.to_string_lossy())
    }

    let profiles_file_path = SettingsKey::ProfileIndexPath.get_string();
    if profiles_file_path.is_empty() {
        SettingsKey::ProfileIndexPath.set_string(&PROFILES_FILE_PATH.to_string_lossy())
    }

    let instances_file_path = SettingsKey::InstanceIndexPath.get_string();
    if instances_file_path.is_empty() {
        SettingsKey::InstanceIndexPath.set_string(&INSTANCES_FILE_PATH.to_string_lossy())
    }
}
